package edu.csu.smartpark;

import edu.csu.smartpark.controller.UserController;
import edu.csu.smartpark.dao.UserDAO;
import edu.csu.smartpark.model.DO.UserDO;
import edu.csu.smartpark.model.PO.ParkingQRCodePO;
import edu.csu.smartpark.model.PO.UserPO;
import edu.csu.smartpark.service.UserService;
import edu.csu.smartpark.util.*;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.*;
import java.util.*;

@SpringBootTest
@Slf4j
class SmartParkApplicationTests {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private OssUtil ossUtil;

    @Autowired
    private UserController userController;

    @Autowired
    private UserService userService;

    @Autowired
    private MongoTemplate mongoTemplate;


    @Test
    void testSelectByMybatisPlus() {
        List<UserPO> userPOList = userDAO.selectList(null);
        userPOList.forEach(System.out::println);
    }


    @Test
    void testJwtUtil(){
        JwtUtil util = new JwtUtil("tom", SignatureAlgorithm.HS256);
        //以tom作为秘钥，以HS256加密
        Map<String, Object> map = new HashMap<>();
        map.put("username", "tom");
        map.put("password", "123456");
        map.put("age", 20);

        String jwtToken = util.encode("tom", 30000, map);

        System.out.println(jwtToken);
        util.decode(jwtToken).entrySet().forEach((entry) -> {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        });
    }

    @Test
    void testUploadOBSObject(){
        String test = "test";
        InputStream stream = new ByteArrayInputStream(test.getBytes());
        String url = ossUtil.putObject("test.txt", stream);
        System.out.println(url);
    }


    @Test
    void testRegister(){
        UserDO userDO = new UserDO();
        userDO.setPhone("12345678904");
        userDO.setEncryptPassword("123456");
        List<String> roles = new ArrayList<>();
        roles.add(Constants.CommonUser);
        userDO.setRoles(roles);
        String id = userService.register(userDO);
        System.out.println(id);

    }

    @Test
    void testStringUtil(){
        String s = "https://smart-park.obs.cn-south-1.myhuaweicloud.com:443/QQ%E6%88%AA%E5%9B%BE20210425161103.jpg";
        System.out.println(ossUtil.getObjectNameByUrl(s));
    }

    @Test
    void updateDataObjectByMybatis(){
        UserPO userPO = new UserPO();
        userPO.setId("8d1757e92faf3339267c71e98f1f5bc5");
        userPO.setNickname("");
        userDAO.updateById(userPO);
    }

    @Test
    void testTemp(){
        Optional<ParkingQRCodePO> test = Optional.ofNullable(null);
        if (test.isPresent()){
            System.out.println("111");
        }
        test.get().getId();
        System.out.println("222");
    }

}
