package edu.csu.smartpark.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.csu.smartpark.model.PO.ParkPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ParkDAO extends BaseMapper<ParkPO> {
}
