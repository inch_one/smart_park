package edu.csu.smartpark.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.csu.smartpark.model.PO.WorkOrderOperationRecordPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WorkOrderOperationRecordDAO extends BaseMapper<WorkOrderOperationRecordPO> {
}
