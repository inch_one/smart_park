package edu.csu.smartpark.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.csu.smartpark.model.PO.ParkSpacePO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ParkSpaceDAO extends BaseMapper<ParkSpacePO> {
}
