package edu.csu.smartpark.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.csu.smartpark.model.PO.ParkingOrderPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ParkingOrderDAO extends BaseMapper<ParkingOrderPO> {
}
