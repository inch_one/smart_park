package edu.csu.smartpark.controller;


import com.alibaba.fastjson.JSONObject;
import edu.csu.smartpark.model.DO.ArticleDO;
import edu.csu.smartpark.model.DTO.ArticleDTO;
import edu.csu.smartpark.model.DTO.ArticleQueryDTO;
import edu.csu.smartpark.model.VO.ArticleVO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.common.BusinessException;
import edu.csu.smartpark.model.common.MsgEntity;
import edu.csu.smartpark.service.ArticleService;
import edu.csu.smartpark.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@Slf4j
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @PostMapping("/add")
    public MsgEntity createArticle(@RequestBody ArticleDTO articleDTO) throws BusinessException {
        if (articleDTO == null){
            return new MsgEntity("success", MsgEntity.SUCCESS, "article info can not be empty");
        }
        if (articleDTO.getType() != Constants.ParkNotice && articleDTO.getType() != Constants.ParkPolicy && articleDTO.getType() != Constants.CommercialInformation){
            return new MsgEntity("success", MsgEntity.SUCCESS, "article type is wrong");
        }
        if (articleDTO.getParkId().equals("")){
            return new MsgEntity("success", MsgEntity.SUCCESS, "park id can not be empty");
        }
        ArticleDO articleDO = new ArticleDO();
        BeanUtils.copyProperties(articleDTO, articleDO);
        String articleId = articleService.createArticle(articleDO);
        if (articleId == null || articleId.equals("")){
            return new MsgEntity("success", MsgEntity.SUCCESS, "create article fail");
        }
        return new MsgEntity("success", MsgEntity.SUCCESS, articleId);
    }

    @GetMapping("/delete")
    public MsgEntity deleteArticle(String articleId) throws BusinessException {
        if (articleId == null || articleId.equals("")){
            return new MsgEntity("success", MsgEntity.SUCCESS, "article id can not be empty");
        }
        int res = articleService.deleteArticle(articleId);
        if (res < 0){
            return new MsgEntity("success", MsgEntity.SUCCESS, "delete article fail");
        }
        return new MsgEntity("success", MsgEntity.SUCCESS, "delete article success");
    }

    @PostMapping("/update")
    public MsgEntity editArticleInfo(@RequestBody ArticleDTO articleDTO) throws BusinessException {
        if (articleDTO == null){
            return new MsgEntity("success", MsgEntity.SUCCESS, "article info can not be empty");
        }
        if (articleDTO.getArticleId() == null || articleDTO.getArticleId().equals("")){
            return new MsgEntity("success", MsgEntity.SUCCESS, "article id is empty, update article fail");
        }
        ArticleDO articleDO = new ArticleDO();
        BeanUtils.copyProperties(articleDTO, articleDO);
        int res = articleService.updateArticleInfo(articleDO);
        if (res < 0){
            return new MsgEntity("success", MsgEntity.SUCCESS,"update article fail");
        }
        return  new MsgEntity("success", MsgEntity.SUCCESS, "update article success");
    }

    @PostMapping("/query")
    public MsgEntity getArticleList(@RequestBody ArticleQueryDTO articleQueryDTO) throws BusinessException {
        PageVO<ArticleVO> res = articleService.queryArticle(articleQueryDTO);
        return new MsgEntity("success", MsgEntity.SUCCESS, res);
    }

    @GetMapping("/info/access")
    public MsgEntity getArticleInfo(String articleId) throws BusinessException {
        if (articleId == null || articleId.equals("")){
            return new MsgEntity("success", MsgEntity.SUCCESS, "article id can not be empty");
        }
        ArticleDO articleDO = articleService.getArticleInfoByArticleId(articleId);
        if (articleDO == null){
            return new MsgEntity("success", MsgEntity.SUCCESS, "get article info fail");
        }
        ArticleVO articleVO = new ArticleVO();
        BeanUtils.copyProperties(articleDO,articleVO);
        return new MsgEntity("success", MsgEntity.SUCCESS, articleVO);
    }

    @PostMapping("/images/upload")
    public MsgEntity uploadArticleImages(MultipartFile[] images) throws BusinessException {
        if (images == null){
            return new MsgEntity("fail", MsgEntity.FAIL, "images is null");
        }
        if (images.length <= 0){
            return new MsgEntity("fail",MsgEntity.FAIL,"image can not be empty");
        }
        List<String> urls = articleService.uploadArticleImages(images);
        return new MsgEntity("success", MsgEntity.SUCCESS, urls);
    }
}
