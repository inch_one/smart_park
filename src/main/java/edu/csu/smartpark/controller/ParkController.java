package edu.csu.smartpark.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.util.BeanUtil;
import edu.csu.smartpark.model.DO.ParkDO;
import edu.csu.smartpark.model.DO.UserDO;
import edu.csu.smartpark.model.DTO.ParkDTO;
import edu.csu.smartpark.model.VO.ParkVO;
import edu.csu.smartpark.model.VO.UserVO;
import edu.csu.smartpark.model.common.BusinessException;
import edu.csu.smartpark.model.common.MsgEntity;
import edu.csu.smartpark.service.ParkService;
import edu.csu.smartpark.util.Constants;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Role;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;

@RestController
@Slf4j
@RequestMapping("/park")
public class ParkController extends BaseController {

    @Autowired
    private ParkService parkService;

    @GetMapping("/all")
    public MsgEntity getAllParkList() {
        return new MsgEntity("Success", MsgEntity.SUCCESS, parkService.getAllParks());
    }

    @GetMapping("/query")
    @RequiresRoles(value = Constants.SystemAdministrator)
    public MsgEntity getParkPage(@RequestParam(value = "pageNum", required = false, defaultValue = "1") long pageNum,
                                 @RequestParam(value = "pageSize", required = false, defaultValue = "10") long pageSize) throws InvocationTargetException, IllegalAccessException {

        return new MsgEntity("success", MsgEntity.SUCCESS, parkService.queryParksForPage(pageSize,pageNum));

    }

    @GetMapping("/get")
    public MsgEntity getParkInfo(@RequestParam("park_id") String parkId) throws BusinessException{
        ParkDO parkDO = parkService.getParkInfoById(parkId);

        log.info("get park_info by id {}", parkId);
        if (parkDO == null) {
            return new MsgEntity("success", MsgEntity.SUCCESS, "park is not exist");
        }

        return new MsgEntity("success", MsgEntity.SUCCESS, parkDO);
    }

    @PostMapping("/update")
    @RequiresRoles(value = {Constants.SystemAdministrator, Constants.ParkAdministrator}, logical = Logical.OR)
    public MsgEntity modifyParkInfo(@RequestBody @Validated ParkDTO parkDTO) throws BusinessException {
        return null;
    }

    @PostMapping("/add")
    @RequiresRoles(value = Constants.SystemAdministrator)
    public MsgEntity createPark(@RequestBody JSONObject params) throws  BusinessException {
        return null;
    }

    @GetMapping("/delete")
    @RequiresRoles(value = Constants.SystemAdministrator)
    public MsgEntity deletePark(@RequestParam("park_id") String parkId) throws BusinessException {
        return null;
    }

}
