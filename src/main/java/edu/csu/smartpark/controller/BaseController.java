package edu.csu.smartpark.controller;

import edu.csu.smartpark.util.JwtUtil;
import edu.csu.smartpark.util.StringEscapeEditor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 类名称：BaseController
 * 类描述：TODO
 * 创建人：CSUpipixia
 * 创建时间：2021/5/8 23:05
 *
 * @version v1.0
 */
@Slf4j
public abstract class BaseController {

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected HttpServletResponse response;

    @Autowired
    private JwtUtil jwtUtil;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringEscapeEditor(true));
    }

    protected String getUserId() {
        String jwt = (String) SecurityUtils.getSubject().getPrincipal();
        return (String) jwtUtil.decode(jwt).get("userId");
    }
}
