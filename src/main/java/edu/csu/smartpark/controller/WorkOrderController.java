package edu.csu.smartpark.controller;

import edu.csu.smartpark.model.DO.WorkOrderDO;
import edu.csu.smartpark.model.DO.WorkOrderOperationRecordDO;
import edu.csu.smartpark.model.DTO.RepairWorkOrderDTO;
import edu.csu.smartpark.model.DTO.WorkOrderOperationRecordDTO;
import edu.csu.smartpark.model.DTO.WorkOrderOperationRecordQueryDTO;
import edu.csu.smartpark.model.DTO.WorkOrderQueryDTO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.VO.WorkOrderOperationRecordVO;
import edu.csu.smartpark.model.VO.WorkOrderVO;
import edu.csu.smartpark.model.common.MsgEntity;
import edu.csu.smartpark.service.WorkOrderService;
import edu.csu.smartpark.util.Constants;
import edu.csu.smartpark.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/work_order")
@Slf4j
@RequiresRoles(value = {Constants.FirstLevelOwnerManager,Constants.SecondLevelOwnerManager, Constants.CompanyStaff}, logical = Logical.OR)
public class WorkOrderController {

    @Autowired
    private WorkOrderService workOrderService;

    @PostMapping("/repair/create")
    public MsgEntity createRepairWorkOrder(@RequestBody RepairWorkOrderDTO repairWorkOrderDTO) {
        if (repairWorkOrderDTO == null){
            return new MsgEntity("fail", MsgEntity.FAIL, "work order param parse fail");
        }
        WorkOrderDO workOrderDO = new WorkOrderDO();
        BeanUtils.copyProperties(repairWorkOrderDTO, workOrderDO);
        workOrderDO.setState(Constants.WorkOrderStatusPending);
        workOrderDO.setTypeCode(Constants.RepairWorkOrder);
        String id = workOrderService.createWorkOrder(workOrderDO);
        return new MsgEntity("success", MsgEntity.SUCCESS, id);
    }

    @PostMapping("/repair/update")
    public MsgEntity modifyRepairWorkOrder(@RequestBody RepairWorkOrderDTO repairWorkOrderDTO){
        if (repairWorkOrderDTO == null){
            return new MsgEntity("fail", MsgEntity.FAIL, "work order param parse fail");
        }
        WorkOrderDO workOrderDO = new WorkOrderDO();
        BeanUtils.copyProperties(repairWorkOrderDTO, workOrderDO);
        workOrderService.updateWorkOrder(workOrderDO);
        return new MsgEntity("success", MsgEntity.SUCCESS, "工单记录更新成功");
    }

    @GetMapping("/delete")
    public MsgEntity deleteWorkOrder(String id) {
        if (StringUtil.isEmpty(id)){
            return new MsgEntity("fail", MsgEntity.FAIL, "work order id can not be empty");
        }
        workOrderService.deleteWorkOrder(id);
        return new MsgEntity("success", MsgEntity.SUCCESS, "工单删除成功");
    }

    @PostMapping("/repair/query")
    @RequiresRoles(value = {Constants.SystemAdministrator, Constants.ParkAdministrator}, logical = Logical.OR)
    public MsgEntity queryRepairWorkOrder(@RequestBody WorkOrderQueryDTO workOrderQueryDTO){
        if (workOrderQueryDTO == null){
            return new MsgEntity("fail", MsgEntity.FAIL, "work order query dto is null");
        }
        PageVO<WorkOrderVO> res = workOrderService.queryWorkOrder(workOrderQueryDTO);
        return new MsgEntity("success", MsgEntity.SUCCESS, res);
    }

    @PostMapping("/operation_record/create")
    @RequiresRoles(value = {Constants.SystemAdministrator, Constants.ParkAdministrator}, logical = Logical.OR)
    public MsgEntity createWorkOrderOperationRecord(@RequestBody WorkOrderOperationRecordDTO workOrderOperationRecordDTO){
        if(workOrderOperationRecordDTO == null){
            return new MsgEntity("fail", MsgEntity.FAIL, "work order operation record is null");
        }
        WorkOrderOperationRecordDO workOrderOperationRecordDO = new WorkOrderOperationRecordDO();
        BeanUtils.copyProperties(workOrderOperationRecordDTO, workOrderOperationRecordDO);
        String id = workOrderService.createWorkOrderOperationRecord(workOrderOperationRecordDO);
        return new MsgEntity("success", MsgEntity.SUCCESS, id);
    }

    @PostMapping("/operation_record/list")
    @RequiresRoles(value = {Constants.SystemAdministrator, Constants.ParkAdministrator}, logical = Logical.OR)
    public MsgEntity getWorkOrderOperationRecordList(@RequestBody WorkOrderOperationRecordQueryDTO workOrderOperationRecordQueryDTO){
        if (workOrderOperationRecordQueryDTO == null){
            log.warn("work order operation record query DTO is null");
            return new MsgEntity("fail", MsgEntity.FAIL, "work order operation record query DTO is null");
        }
        List<WorkOrderOperationRecordDO> workOrderOperationRecordDOS = workOrderService.getWorkOrderOperationRecords(workOrderOperationRecordQueryDTO);
        List<WorkOrderOperationRecordVO> workOrderOperationRecordVOS = new LinkedList<>();
        for (WorkOrderOperationRecordDO workOrderOperationRecordDO : workOrderOperationRecordDOS){
            WorkOrderOperationRecordVO workOrderOperationRecordVO = new WorkOrderOperationRecordVO();
            BeanUtils.copyProperties(workOrderOperationRecordDO, workOrderOperationRecordVO);
            workOrderOperationRecordVOS.add(workOrderOperationRecordVO);
        }
        return new MsgEntity("success", MsgEntity.SUCCESS, workOrderOperationRecordVOS);
    }
}
