package edu.csu.smartpark.controller;

import edu.csu.smartpark.model.DO.ParkingOrderDO;
import edu.csu.smartpark.model.DTO.PayOrderDTO;
import edu.csu.smartpark.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping("/pay_result/callback")
    public void payResultCallback(@RequestBody PayOrderDTO payOrderDTO){
        if (payOrderDTO == null){
            log.warn("[payResultCallback] request param parse error");
            return;
        }
        ParkingOrderDO parkingOrderDO = new ParkingOrderDO();
        parkingOrderDO.setStatus(payOrderDTO.getState());
        parkingOrderDO.setId(payOrderDTO.getMchOrderNo());
        orderService.updateParkingOrder(parkingOrderDO);
    }
}
