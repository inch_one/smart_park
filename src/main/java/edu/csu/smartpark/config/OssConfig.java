package edu.csu.smartpark.config;

import edu.csu.smartpark.model.properties.HuaweiOBSProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
* @Description: 华为云obs相关配置加载
* @Author: LZY
* @Date: 2021/4/13 12:22
*/
@Configuration
public class OssConfig {

    @Bean
    @ConfigurationProperties(prefix = "huawei.obs")
    public HuaweiOBSProperties getHuaweiOBSProperties(){
        return  new HuaweiOBSProperties();
    }

}
