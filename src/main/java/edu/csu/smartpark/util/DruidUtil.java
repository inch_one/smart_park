package edu.csu.smartpark.util;


import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/*
* @Description: druid相关工具函数
* @Author: LZY
* @Date: 2021/4/12 20:30
*/
public class DruidUtil {

    @Autowired
    private static DataSource druidDataSource;

    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = druidDataSource.getConnection();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

}
