package edu.csu.smartpark.util;

import com.obs.services.ObsClient;
import com.obs.services.model.ObjectMetadata;
import com.obs.services.model.ObsObject;
import com.obs.services.model.PutObjectResult;
import edu.csu.smartpark.model.properties.HuaweiOBSProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

/*
* @Description: 连接阿里云oss，上传下载文件工具包
* @Author: LZY
* @Date: 2021/4/13 14:23
*/
@Component
@Slf4j
public class OssUtil {

    @Autowired
    private HuaweiOBSProperties properties;

    public ObsClient getClient(){
        ObsClient client = new ObsClient(properties.getObjectAk(), properties.getObjectSk(), properties.getObjectEndpoint());
        if (client == null){
            log.warn("Obs client open fail");
        }
        return client;
    }

    public String  putObject(String objectName, InputStream inputStream){
        ObsClient client = getClient();
        if (client != null){
            PutObjectResult result = client.putObject(properties.getObjectBucket(), objectName, inputStream);
            log.info(String.format("OSS put object: %s success", objectName));
            try {
                client.close();
            }catch (Exception e){
                log.warn("obs client close fail");
            }
            return result.getObjectUrl();
        }
        log.info(String.format("OSS put object: %s fail", objectName));
        return "";
    }

    public InputStream getObject(String objectName){
        ObsClient client = getClient();
        if (client != null){
            ObsObject object = client.getObject(properties.getObjectBucket(), objectName);
            InputStream content = object.getObjectContent();
            try {
                client.close();
            }catch (Exception e){
                log.warn("obs client close fail");
            }
            log.info(String.format("Oss get object: %s success", objectName));
            return content;
        }
        log.info(String.format("Oss get object: %s fail", objectName));
        return null;
    }

    public void deleteObject(String objectName){
        ObsClient client = getClient();
        if (client != null){
            client.deleteObject(properties.getObjectBucket(), objectName);
            log.info(String.format("Oss delete object: %s success", objectName));
            try {
                client.close();
            }catch (Exception e){
                log.warn("obs client close fail");
            }
            return;
        }
        log.info(String.format("Oss delete object: %s fail", objectName));
    }

    /*
    * @Description: 上传图像文件
    * @Author: LZY
    * @Date: 2021/4/21 15:50
    * @Params:
    * @Return:
    */
    public PutObjectResult uploadImage(MultipartFile file) {
        ObsClient client = getClient();
        try {
            if (file == null || file.getOriginalFilename() == ""){
                log.warn("file or file name can not be empty, upload image fail");
                return null;
            }
            String objectName = file.getOriginalFilename();
            InputStream stream = file.getInputStream();
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.getSize());
            PutObjectResult result = client.putObject(properties.getObjectBucket(), objectName, stream, metadata);
            return result;
        }catch (Exception e){
            log.warn("input image file stream exception, upload image fail");
            return null;
        }
    }

    public String getObjectNameByUrl(String url){
        String decodeUrl = StringUtil.URLDecoderString(url);
        List<String> temp = StringUtil.splits(decodeUrl, "/");
        return temp.get(temp.size()-1);
    }
}
