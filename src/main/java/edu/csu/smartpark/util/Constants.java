package edu.csu.smartpark.util;

/*
* @Description: 项目常量定义类
* @Author: LZY
* @Date: 2021/4/13 19:07
*/
public class Constants {
    // 角色类型
    public static final String SystemAdministrator = "ROLE_ADMIN";
    public static final String ParkAdministrator = "ROLE_P_ADMIN";
    public static final String FirstLevelOwnerManager = "ROLE_B_ADMIN";
    public static final String SecondLevelOwnerManager = "ROLE_B_ASSISTANT";
    public static final String CompanyStaff = "ROLE_B_STAFF";
    public static final String CommonUser = "ROLE_USER";

    // 登录类型
    public static final String PhonePassword = "1";
    public static final String PhoneCode = "2";

    // 验证码类型
    public static final String PhoneCodeForLogin = "1";
    public static final String PhoneCodeForRegister = "2";
    public static final String PhoneCodeForForgetPassword = "3";
    public static final String PhoneCodeForPhoneModification = "4";

    // 文章类型
    public static final int ParkNotice = 1; // 园区公告
    public static final int ParkPolicy = 2; // 园区政策
    public static final int CommercialInformation = 3; // 招商信息

    // 空间类型
    public static final String area = "区域";
    public static final String building = "幢";
    public static final String unit = "单元";
    public static final String house = "房屋";
    public static final String parkingArea = "停车区域";
    public static final String parkSpaceChannel = "车道空间";

    // 工单状态类型
    public static final Integer WorkOrderStatusPending = 1; // 待处理
    public static final Integer WorkOrderStatusProcessing = 2; // 处理中
    public static final Integer WorkOrderStatusProcessed = 3; // 已处理
    public static final Integer WorkOrderStatusCanceled = 4; // 已取消

    // 工单类型
    public static final Integer RepairWorkOrder = 1; // 维修工单

    // 企业状态类型
    public static final Integer EnterpriseStatusReject = -1; //企业状态-驳回
    public static final Integer EnterpriseStatusPendingApproval = 0; //企业状态-待审核
    public static final Integer EnterpriseStatusNormal = 1; //企业状态-正常
    public static final Integer EnterpriseStatusFrozen = 2; //企业状态-冻结
    public static final Integer EnterpriseStatusCanceled = 3; //企业状态-已取消

    // 与支付平台相关的常量
    public static final String MchNo = ""; // 商户号
    public static final String AppId = ""; // 应用ID
    public static final String NotifyUrl = ""; // 支付结果回调url
    public static final String Version = "1.0"; // 接口版本号
    public static final String SignType = "MD5"; // 签名类型
    public static final String PrivateKey = ""; // 商户私钥
}
