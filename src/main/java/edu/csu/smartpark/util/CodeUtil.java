package edu.csu.smartpark.util;

import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CodeUtil {
    public static final String EMAILHOSTNAME = "smtp.163.com";
    public static final String EMAILSENDER = "lzy_cs2021@163.com";
    public static final String EMAILPASSWORD = "SXJIGMJKAEBZCIRD";


    @Autowired
    private RedisUtil redisUtil;

    /*
    * @Description: 发送短信验证码
    * @Author: LZY
    * @Date: 2021/4/25 15:37
    * @Params: type 用于区分验证码发送类型，1-登录，2-忘记密码
    * @Return:
    */
    public String sendCode(String phone, String type){
        String code = generateCode();
        // TODO 向手机发送验证码
        // 验证码有效时间5分钟
        StringBuilder key = new StringBuilder("phone_code_");
        key.append(phone);
        if (type.equals(Constants.PhoneCodeForLogin)){
            // 使用登录信息模板发送验证码
           key.append(Constants.PhoneCodeForLogin);
        } else if (type.equals(Constants.PhoneCodeForRegister)){
            // 使用注册信息模板发送验证码
            key.append(Constants.PhoneCodeForRegister);
        } else if (type.equals(Constants.PhoneCodeForForgetPassword)){
            // 使用忘记密码信息模板发送验证码
           key.append(Constants.PhoneCodeForForgetPassword);
        } else {
            // 使用修改手机号码的信息模板发送验证码
            key.append(Constants.PhoneCodeForPhoneModification);
        }
        redisUtil.set(key.toString(), code, 300);
        return code;
    }

    /*
    * @Description: 根据手机号码获取短信验证码
    * @Author: LZY
    * @Date: 2021/4/23 18:34
    * @Params: 如果没有获取到验证法，返回的是null
    * @Return:
    */
    public String getCode(String phone, String type){
        StringBuilder key = new StringBuilder("phone_code_");
        key.append(phone);
        if (type.equals(Constants.PhoneCodeForLogin)){
            // 使用登录信息模板发送验证码
            key.append(Constants.PhoneCodeForLogin);
        } else if (type.equals(Constants.PhoneCodeForRegister)){
            // 使用注册信息模板发送验证码
            key.append(Constants.PhoneCodeForRegister);
        } else if (type.equals(Constants.PhoneCodeForForgetPassword)){
            // 使用忘记密码信息模板发送验证码
            key.append(Constants.PhoneCodeForForgetPassword);
        } else {
            // 使用修改手机号码的信息模板发送验证码
            key.append(Constants.PhoneCodeForPhoneModification);
        }
        return (String)redisUtil.get(key.toString());
    }

    /*
    * @Description: 根据邮箱发送验证码
    * @Author: LZY
    * @Date: 2021/4/25 17:09
    * @Params:
    * @Return:
    */
    public String sendEmailCode(String emailNumber){
        try {
            String code = generateCode();
            HtmlEmail email = new HtmlEmail();
            email.setHostName(EMAILHOSTNAME);
            email.setCharset("utf-8");
            email.addTo(emailNumber);
            email.setFrom(EMAILSENDER, "smart-park");
            email.setAuthentication(EMAILSENDER, EMAILPASSWORD);
            email.setSubject("[smart park] modify email number");
            email.setMsg(code);
            email.send();
            StringBuilder key = new StringBuilder("email_code_");
            key.append(emailNumber);
            redisUtil.set(key.toString(), code);
            return code;
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    /*
    * @Description: 获取邮箱验证码
    * @Author: LZY
    * @Date: 2021/4/25 17:32
    * @Params:
    * @Return:
    */
    public String getEmailCode(String emailNumber){
        StringBuilder key = new StringBuilder("email_code_");
        key.append(emailNumber);
        return (String)redisUtil.get(key.toString());
    }

    /*
     * @Description: 随机生成六位数验证码
     * @Author: LZY
     * @Date: 2021/4/16 11:26
     * @Params:
     * @Return:
     */
    private String generateCode() {
        String random = String.valueOf((int)((Math.random()*9+1)*Math.pow(10, 5)));
        return random;
    }
}
