package edu.csu.smartpark.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class MongoDBPageable implements Pageable {
    private int pageNum = 1;
    private int pageSize = 10;
    private Sort sort;

    public void setCurrentPage(int pageNum){
        this.pageNum = pageNum;
    }

    @Override
    public int getPageNumber() {
        return pageNum;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize){
        this.pageSize = pageSize;
    }

    @Override
    public long getOffset() {
        return (pageNum-1)*pageSize;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort){
        this.sort = sort;
    }

    @Override
    public Pageable next() {
        return PageRequest.of(pageNum + 1, pageSize, sort);
    }

    @Override
    public Pageable previousOrFirst() {
        return pageNum == 0 ? this : PageRequest.of(pageNum - 1, pageSize , sort);
    }

    @Override
    public Pageable first() {
        return PageRequest.of(0, pageSize , sort);
    }

    @Override
    public boolean hasPrevious() {
        return pageNum > 1;
    }
}
