package edu.csu.smartpark.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import edu.csu.smartpark.util.CommonUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createTime", Date.class, CommonUtil.getCurrentTime());
        this.strictInsertFill(metaObject, "updateTime", Date.class, CommonUtil.getCurrentTime());
        this.strictInsertFill(metaObject, "isDeleted", Integer.class, 0);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "updateTime", Date.class, CommonUtil.getCurrentTime());
    }
}
