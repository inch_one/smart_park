package edu.csu.smartpark.model.properties;

/*
* @Description: 华为云OBS属性对象
* @Author: LZY
* @Date: 2021/4/21 10:00
*/

public class HuaweiOBSProperties {
    private String objectEndpoint;

    private String objectBucket;

    private String objectAk;

    private String objectSk;


    public String getObjectEndpoint() {
        return objectEndpoint;
    }

    public void setObjectEndpoint(String objectEndpoint) {
        this.objectEndpoint = objectEndpoint;
    }

    public String getObjectBucket() {
        return objectBucket;
    }

    public void setObjectBucket(String objectBucket) {
        this.objectBucket = objectBucket;
    }

    public String getObjectAk() {
        return objectAk;
    }

    public void setObjectAk(String objectAk) {
        this.objectAk = objectAk;
    }

    public String getObjectSk() {
        return objectSk;
    }

    public void setObjectSk(String objectSk) {
        this.objectSk = objectSk;
    }
}
