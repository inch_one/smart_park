package edu.csu.smartpark.model.DTO;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class TollMonthCardProductDTO {
    private String cardName;
    private String briefIntroduction;
    private Integer availableCarType;
    private Integer availableTime;
    private Double monthlyFee;
    private String parkId;
    private Integer status;
}
