package edu.csu.smartpark.model.DTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class RegisterDTO {
    @NotBlank(message = "phone can not be empty")
    @Pattern(regexp="^1[0-9]{10}$", message = "phone number format is wrong")
    private String phone;

    @NotBlank(message = "password can not be empty")
    @Size(min = 6, max = 20, message = "password is to long or too short")
    private String password;

    @NotBlank(message = "confirm password can not be empty")
    private String confirmPassword;

    @NotBlank(message = "verify code can not be empty")
    private String code;

}
