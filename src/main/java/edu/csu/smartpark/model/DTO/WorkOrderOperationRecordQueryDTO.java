package edu.csu.smartpark.model.DTO;

import lombok.Data;

@Data
public class WorkOrderOperationRecordQueryDTO {
    private String id;
    private String workOrderId;
    private Integer operationType;
}
