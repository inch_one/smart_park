package edu.csu.smartpark.model.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.csu.smartpark.model.DO.BillContentDO;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class BillDTO {
    @NotBlank(message = "park id can not be empty")
    private String parkId;
    @NotBlank(message = "enterprise can not be empty")
    private String enterpriseId;
    private String enterpriseName;
    @NotEmpty(message = "bill content can not be empty")
    private List<BillContentDO> billContents;
    private double payableTotal;
    private double reducedTotal;
    @NotNull(message = "实付总额不可为空")
    private double actualTotal;
    private String remark;
    @NotNull(message = "charge date can not be empty")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date chargeDate; // 收费日期
    private String paymentDocumentUrl; // 支付凭证截图url
}
