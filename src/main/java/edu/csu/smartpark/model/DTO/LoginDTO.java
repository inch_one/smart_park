package edu.csu.smartpark.model.DTO;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginDTO {
    @NotBlank(message = "phone can not be empty")
    private String phone;
    @NotBlank(message = "password can not be empty")
    private String password;
    @NotBlank(message = "login type my be chosen")
    private String loginType;
}
