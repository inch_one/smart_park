package edu.csu.smartpark.model.DTO;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ParkSpaceDTO {
    @NotNull(message = "space name can not be empty")
    private String name;
    @NotNull(message = "parent id can not be empty")
    private String parentId;
    @NotNull(message = "type code can not be empty")
    private String typeCode;
    private Integer direction;
    private Integer roomSize;
    private Integer roomCapacity;
    private String tollStandardId;
}
