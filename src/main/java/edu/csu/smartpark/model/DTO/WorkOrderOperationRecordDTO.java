package edu.csu.smartpark.model.DTO;

import lombok.Data;

import java.util.Date;

@Data
public class WorkOrderOperationRecordDTO {
    private String workOrderId;
    private Integer operationType;
    private String feedback;
}
