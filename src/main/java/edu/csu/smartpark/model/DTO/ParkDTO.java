package edu.csu.smartpark.model.DTO;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 类名称：ParkDTO
 * 类描述：搜索园区接收参数
 * 创建人：CSUpipixia
 * 创建时间：2021/4/27 21:25
 *
 * @version v1.0
 */

@Data
public class ParkDTO {

    @NotBlank
    private String id;
    private String parkName;
    private String briefIntroduction;
    private String introduction;
    private List<MultipartFile> parkImage;
    private String contactDetails;
    private String province;
    private String city;
    private String area;
    private String address;
    private Double longitude;
    private Double latitude;
}
