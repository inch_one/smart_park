package edu.csu.smartpark.model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * 类名称：EnterpriseDTO
 * 类描述：TODO
 * 创建人：CSUpipixia
 * 创建时间：2021/5/12 20:13
 *
 * @version v1.0
 */
@Data
public class EnterpriseDTO {

    private String code;
    private String name;
    private MultipartFile logo;
    private String briefIntroduction;
    private String phone;
    // 企业类型，根据接口位置判断，app端或者园区管理端只能为公司，自己的管理端可以选择【1-园区，2-公司】
    private int type;
    private String address;
    private String legalPersonName;
    private String establishmentDate;
    private MultipartFile businessLicense;
    private String businessScope;
    private MultipartFile legalPersonCertificate;
    private String identityNumber;
    // 园区id
    private String parkId;
    private Date entryTime;
}
