package edu.csu.smartpark.model.DTO;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;

/**
 * 类名称：EnterpriseBasicDTO
 * 类描述：企业基本信息DTO
 * 创建人：CSUpipixia
 * 创建时间：2021/7/15 15:14
 *
 * @version v1.0
 */
@Data
public class EnterpriseBasicDTO {
    @NotBlank(message = "id can not be empty")
    private String id;
    private String name;
    private String briefIntroduction;
    private String phone;
    private String address;
    private Double longitude;
    private Double latitude;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String entryTime;
}
