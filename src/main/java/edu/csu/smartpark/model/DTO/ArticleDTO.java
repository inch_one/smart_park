package edu.csu.smartpark.model.DTO;

import lombok.Data;

import java.util.List;

@Data
public class ArticleDTO {
    private int type;
    private String title;
    private String articleId;
    private List<String> coverImageUrl;
    private String parkId;
    private String publisherId;
    private int viewNum;
    private String text;
    private List<String> imageUrls;
}
