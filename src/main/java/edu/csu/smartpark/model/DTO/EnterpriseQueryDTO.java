package edu.csu.smartpark.model.DTO;

import lombok.Data;

@Data
public class EnterpriseQueryDTO {
    private Integer pageSize;
    private Integer pageNum;
    private String name;
    private String code;
    private Integer type;
    private String parkId;
    private Integer status;
}
