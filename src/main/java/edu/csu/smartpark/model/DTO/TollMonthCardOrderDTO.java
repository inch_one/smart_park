package edu.csu.smartpark.model.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class TollMonthCardOrderDTO {
    private String license;
    private Integer orderType;
    private String tollMonthCardId;
    private String monthCardProductId;
    private Integer availableCarType;
    private Integer availableTime;
    private Double orderTotal;
    private Double discountAmount;
    private Double payableAmount;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date oldIndate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date newIndate;
    private String parkId;
    private String buyerId;
    private String wayCode;
}
