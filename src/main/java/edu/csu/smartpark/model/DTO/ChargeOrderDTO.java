package edu.csu.smartpark.model.DTO;

import lombok.Data;

/**
 * @author SHW
 * @date 2021/7/14 1:04
 */
@Data
public class ChargeOrderDTO {
    private Integer pageNum;
    private Integer pageSize;
    private String orderId;
    private String parkId;
    private String startTime;
    private String endTime;
    private Integer status;
}
