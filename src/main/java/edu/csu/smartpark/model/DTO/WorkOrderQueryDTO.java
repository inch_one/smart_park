package edu.csu.smartpark.model.DTO;

import lombok.Data;

import java.util.List;

@Data
public class WorkOrderQueryDTO {
    private Integer pageNum;
    private Integer pageSize;
    private String id;
    private String applicantName;
    private String applicantPhone;
    private String applicantId;
    private Integer typeCode;
    private String parkId;
    private List<Integer> state;
}
