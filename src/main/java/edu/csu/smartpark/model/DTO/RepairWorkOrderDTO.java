package edu.csu.smartpark.model.DTO;

import edu.csu.smartpark.model.DO.RepairWorkOrderDO;
import lombok.Data;

@Data
public class RepairWorkOrderDTO {
    private String id;
    private String applicantName;
    private String applicantPhone;
    private String applicantId;
    private RepairWorkOrderDO workOrderSpecific;
    private String parkId;
    private Integer state;
}
