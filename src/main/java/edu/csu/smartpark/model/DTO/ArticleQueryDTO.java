package edu.csu.smartpark.model.DTO;

import lombok.Data;

import java.util.List;

@Data
public class ArticleQueryDTO {
    private Integer pageNum;
    private Integer pageSize;
    private String parkId;
    private List<Integer> articleType;
}
