package edu.csu.smartpark.model.DTO;

import lombok.Data;

@Data
public class TollMonthCardProductQueryDTO {
    private String productId;
    private String cardName;
    private int availableCarType;
    private int  availableTime;
    private String parkId;
    private int status;
}
