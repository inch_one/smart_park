package edu.csu.smartpark.model.DTO;
import lombok.Data;

@Data
public class PayOrderDTO {
    private String payOrderId;
    private String mchNo;
    private String appId;
    private String mchOrderNo;
    private String ifCode; // 支付接口编码
    private String wayCode; // 支付方式
    private Double amount; // 支付金额
    private String currency;
    private Integer state;
    private String subject; // 商品标题
    private String body; // 商品描述
    private String sign;
}
