package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class TollMonthCardOrderDO {
    private String id;
    private String license;
    private Integer orderType;
    private Integer availableCarType;
    private Integer availableTime;
    private Double orderTotal;
    private Double discountAmount;
    private Double payableAmount;
    private Date payTime;
    private Date oldIndate;
    private Date newIndate;
    private String monthCardProductId;
    private String tollMonthCardId;
    private String parkId;
    private String buyerId;
    private Integer status;
    private String wayCode;
}
