package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserDO implements Serializable {
    private String id;
    private String nickname;
    private String realName;
    private String phone;
    private String email;
    private String encryptPassword;
    private String avatarUrl;
    private int age;
    private int gender;
    private List<String> roles;
    private String enterpriseId;
    private String parkId;
    private int status;
}
