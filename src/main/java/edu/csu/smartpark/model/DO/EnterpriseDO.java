package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EnterpriseDO {
    private String id;
    private String code;
    private String name;
    private String logoUrl;
    private String briefIntroduction;
    private List<String> pictureUrls;
    private String phone;
    private String introduction;
    private int type;
    private String address;
    private Double longitude;
    private Double latitude;
    // 敏感信息
//    private String legalPersonName;
//    private String establishmentDate;
//    private String businessLicenseUrl;
//    private String businessScope;
//    private String legalPersonCertificateUrl;
//    private String identityNumber;
    private String parkId;
    private Date entryTime;
    private int status;
    private Date createTime;
    private Date updateTime;
}
