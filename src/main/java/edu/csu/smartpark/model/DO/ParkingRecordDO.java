package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class ParkingRecordDO {
    private String id;
    private String license;
    private Integer vehicleType;
    private Integer vehicleState;
    private Date entryTime;
    private String entryPictureUrl;
    private String entrySpaceName;
    private String entrySpaceId;
    private Date outTime;
    private String outPictureUrl;
    private String outSpaceName;
    private String outSpaceId;
    private Integer parkingTime;
    private Double tollFee;
    private String paymentType;
    private String parkId;
    private Integer status;
    private String event;
    private String remark;
}
