package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class WorkOrderOperationRecordDO {
    private String id;
    private String workOrderId;
    private Integer operationType;
    private String feedback;
    private Date updateTime;
}
