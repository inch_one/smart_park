package edu.csu.smartpark.model.DO;

import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class ArticleDO {
    private int type;
    private String title;
    private String articleId;
    private List<String> coverImageUrl;
    private Date releaseTime;
    private String parkId;
    private String publisherId;
    private int viewNum;
    private int isDeleted;
    private String text;
    private List<String> imageUrls;
}
