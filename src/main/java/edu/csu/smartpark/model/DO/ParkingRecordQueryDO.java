package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class ParkingRecordQueryDO {
    private String id;
    private String license;
    private Integer vehicleType;
    private Integer vehicleState;
    private String parkId;
    private Integer status;
    private Date startTime;
    private Date endTime;
}
