package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class ParkingOrderDO {
    private String id;
    private String inoutId;
    private String license;
    private Double orderTotal;
    private Double discountAmount;
    private Double payableAmount;
    private Date payTime;
    private String parkId;
    private Integer status;
    private String wayCode;
}
