package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class ParkDO implements Serializable {
    private String id;
    private String parkName;
    private List<String> parkImageUrlList;
    private String briefIntroduction;
    private String introduction;
    private String contactDetails;
    private String province;
    private String city;
    private String area;
    private String address;
    private Double longitude;
    private Double latitude;
    private String enterpriseId;
    private int status;
    private Date createTime;
    private Date updateTime;
}
