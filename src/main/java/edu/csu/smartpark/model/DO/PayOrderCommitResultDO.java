package edu.csu.smartpark.model.DO;

import lombok.Data;


/*
* @Description: 向第三方支付平台提交支付订单生成请求后得到的响应参数
* @Author: LZY
* @Date: 2021/6/27 9:33
*/
@Data
public class PayOrderCommitResultDO {
    private String sign;
    private String payOrderId;
    private String mchOrderNo;
    private Integer orderState;
    private String payDataType;
    private String payData;
}
