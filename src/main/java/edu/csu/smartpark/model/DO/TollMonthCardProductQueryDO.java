package edu.csu.smartpark.model.DO;

import lombok.Data;

@Data
public class TollMonthCardProductQueryDO {
    private String id;
    private String cardName;
    private Integer availableCarType;
    private Integer availableTime;
    private String parkId;
    private Integer status;
}
