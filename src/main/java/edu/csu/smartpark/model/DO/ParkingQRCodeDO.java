package edu.csu.smartpark.model.DO;

import lombok.Data;

@Data
public class ParkingQRCodeDO {
    private String id;
    private String spaceId;
    private String deviceId;
    private String imageStr;
}
