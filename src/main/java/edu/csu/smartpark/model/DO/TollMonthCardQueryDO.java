package edu.csu.smartpark.model.DO;

import lombok.Data;

@Data
public class TollMonthCardQueryDO {
    private String id;
    private String license;
    private String buyerId;
    private Integer status;
    private String parkId;
}
