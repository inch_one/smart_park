package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class BillTypeDO {
    private String id;
    private String name;
    private Date createTime;
    private Date updateTime;
}
