package edu.csu.smartpark.model.DO;

import lombok.Data;

@Data
public class ParkSpaceDO {
    private String spaceId;
    private String name;
    private String parentId;
    private String typeCode;
    private String path;
    private Integer status;
    private Integer direction;
    private Integer roomSize;
    private Integer roomCapacity;
    private String tollStandardId;
    private String remark;
}
