package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
/*
* @Description: 支付订单领域对象
* @Author: LZY
* @Date: 2021/6/29 10:00
*/
public class PayOrderDO {
    private String payOrderId;
    private String mchNo;
    private String appId;
    private String mchOrderNo;
    private String ifCode; // 支付接口编码
    private String wayCode; // 支付方式
    private Double amount; // 支付金额
    private String currency;
    private Integer state;
    private String subject; // 商品标题
    private String body; // 商品描述
    private String notifyUrl;
    private String sign;
    private Date createTime; // 订单创建时间
    private Date successTime; // 订单支付成功时间
}
