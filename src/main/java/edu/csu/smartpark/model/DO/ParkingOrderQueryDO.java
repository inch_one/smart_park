package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class ParkingOrderQueryDO {
    private String id;
    private String license;
    private String inoutId;
    private Date startTime;
    private Date endTime;
    private Integer status;
}
