package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class TollMonthCardOrderQueryDO {
    private String id;
    private String license;
    private Integer orderType;
    private String buyerId;
    private Date startTime;
    private Date endTime;
    private Integer status;
}
