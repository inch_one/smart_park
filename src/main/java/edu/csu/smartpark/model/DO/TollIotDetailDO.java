package edu.csu.smartpark.model.DO;

import lombok.Data;

@Data
public class TollIotDetailDO {
    private String id;
    private String deviceId;
    private String deviceName;
    private String spaceId;
    private String spaceName;
    private String parkId;
    private String parkingAreaId;
}
