package edu.csu.smartpark.model.DO;

import com.alibaba.fastjson.JSONObject;
import edu.csu.smartpark.model.DO.BaseDO.BaseWorkOrderSpecific;
import edu.csu.smartpark.model.common.BusinessException;
import edu.csu.smartpark.util.StringUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @date 2021/7/17  11:43
 */
@Data
@Slf4j
public class RepairWorkOrderDO extends BaseWorkOrderSpecific {
    private String repairAddress;
    private String repairType;
    private String repairSpecific;

    @Override
    public String updateSpecific(String workOrderSpecific) throws BusinessException {
        RepairWorkOrderDO repairWorkOrderDO = JSONObject.parseObject(workOrderSpecific, RepairWorkOrderDO.class);
        if (repairWorkOrderDO == null){
            log.warn("parse work order specific string to repairWorkOrderDO fail");
            throw new BusinessException(BusinessException.INTERNAL_SERVER_ERROR, "parse json str error");
        }
        if (!StringUtil.isEmpty(this.getRepairSpecific())){
            repairWorkOrderDO.setRepairSpecific(this.getRepairSpecific());
        }
        if (!StringUtil.isEmpty(this.getRepairAddress())){
            repairWorkOrderDO.setRepairAddress(this.getRepairAddress());
        }
        if (!StringUtil.isEmpty(this.getRepairType())){
            repairWorkOrderDO.setRepairType(this.getRepairType());
        }
        return JSONObject.toJSONString(repairWorkOrderDO);
    }
}
