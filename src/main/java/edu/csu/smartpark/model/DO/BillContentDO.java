package edu.csu.smartpark.model.DO;

import lombok.Data;

@Data
public class BillContentDO {
    private String id;
    private String name; // 费用名称
    private String detail;
    private double payableAmount;
    private double reducedAmount;
    private double actualAmount;
}
