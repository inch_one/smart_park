package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class LicensePlateResultDO {
    private String deviceName;
    private String ipAddr;
    private String deviceId;
    private String license;
    private Integer vehicleType;
    private Date receivedTime; // 接收请求的时间
    private String plateId;
    private Integer isFakePlate;
    private String snapImageUrl; // 车辆截图URL
}
