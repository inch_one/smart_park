package edu.csu.smartpark.model.DO.BaseDO;

/*
 * Introduction： 工单详情的基类，不同类型的工单需要继承自此类
 * @date 2021/7/17 11:42
 */
public abstract class BaseWorkOrderSpecific {
    public abstract String updateSpecific(String workOrderSpecific);
}
