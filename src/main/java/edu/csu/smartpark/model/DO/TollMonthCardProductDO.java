package edu.csu.smartpark.model.DO;

import lombok.Data;

@Data
public class TollMonthCardProductDO {
    private String id;
    private String cardName;
    private String briefIntroduction;
    private Integer availableCarType;
    private Integer availableTime;
    private Double monthlyFee;
    private String parkId;
    private Integer status;

}
