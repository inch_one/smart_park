package edu.csu.smartpark.model.DO;

import lombok.Data;

import java.util.Date;

@Data
public class TollMonthCardDO {
    private String id;
    private String cardName;
    private String license;
    private String briefIntroduction;
    private Integer availableCarType;
    private Integer availableTime;
    private Date dueTime;
    private String parkId;
    private String buyerId;
    private Integer status;
}
