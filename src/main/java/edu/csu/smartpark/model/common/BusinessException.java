package edu.csu.smartpark.model.common;

public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = -7480022450501760611L;

    // 定义常用异常码
    public static final String LOGINERROR = "901";
    public static final String REGISTERERROR = "902";
    public static final String PARAMETERERROR = "903";

    public static final String SERVERERROR = "801";

    /**
     * 未认证（签名错误）
     */
    public static final String UNAUTHORIZED = "401";

    /**
     * 接口、资源不存在
     */
    public static final String NOT_FOUND = "404";

    /**
     * 请求超时
     */
    public static final String REQUEST_TIMEOUT = "408";

    /**
     * 资源冲突
     */
    public static final String CONFLICT = "409";

    /**
     * 媒介类型不被支持
     */
    public static final String UNSUPPORTED_MEDIA_TYPE = "415";

    /**
     * 服务器内部错误
     */
    public static final String INTERNAL_SERVER_ERROR = "500";

    /**
     * 请求未完成。服务器从上游服务器收到一个无效的响应
     */
    public static final String BAD_GATEWAY = "502";

    /**
     * 异常码
     */
    private String code;

    /**
     * 异常提示信息
     */
    private String message;

    public BusinessException() {
    }

    public BusinessException(String code, String msg) {
        this.code = code;
        this.message = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
