package edu.csu.smartpark.model.common;

public class MsgEntity {

    /**
     * 成功
     */
    public static final String SUCCESS = "200";

    /**
     * 失败, 服务器未能理解请求
     */
    public static final String FAIL = "200";

    /**
     * 未认证（签名错误）
     */
    public static final String UNAUTHORIZED = "401";

    /**
     * 接口不存在
     */
    public static final String NOT_FOUND = "404";

    /**
     * 请求超时
     */
    public static final String REQUEST_TIMEOUT = "408";

    /**
     * 资源冲突
     */
    public static final String CONFLICT = "409";

    /**
     * 媒介类型不被支持
     */
    public static final String UNSUPPORTED_MEDIA_TYPE = "415";

    /**
     * 服务器内部错误
     */
    public static final String INTERNAL_SERVER_ERROR = "500";

    /**
     * 请求未完成。服务器从上游服务器收到一个无效的响应
     */
    public static final String BAD_GATEWAY = "502";

    private String state;
    private String code;
    private Object msg;

    public MsgEntity(String state, String code, Object msg) {
        this.state = state;
        this.code = code;
        this.msg = msg;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }
}
