package edu.csu.smartpark.model.PO;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@TableName("user")
@Data
public class UserPO implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String nickname;
    private String realName;
    private String phone;
    private String email;
    private String encryptPassword;
    private String avatarObjectName;
    private String avatarUrl;
    private int age;
    private int gender;
    private String roles;
    private String enterpriseId;
    private String parkId;
    private int status;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
}
