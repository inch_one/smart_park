package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("toll_iot_detail")
public class TollIotDetailPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String deviceId;
    private String deviceName;
    private String spaceId;
    private String spaceName;
    private String parkId;
    private String parkingAreaId;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
