package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("park_info")
public class ParkPO implements Serializable {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String parkName;
    private String parkImageUrl;
    private String briefIntroduction;
    private String introduction;
    private String contactDetails;
    private String province;
    private String city;
    private String area;
    private String address;
    private Double longitude;
    private Double latitude;
    private String enterpriseId;
    private int status;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
