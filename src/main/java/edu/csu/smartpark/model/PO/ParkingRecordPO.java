package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("toll_inout_detail")
public class ParkingRecordPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String license;
    private Integer vehicleType;
    private Integer vehicleState;
    private Date entryTime;
    private String entryPictureUrl;
    private String entrySpaceName;
    private String entrySpaceId;
    private Date outTime;
    private String outPictureUrl;
    private String outSpaceName;
    private String outSpaceId;
    private Integer parkingTime;
    private Double tollFee;
    private String paymentType;
    private String parkId;
    private Integer status;
    private String event;
    private String remark;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
}
