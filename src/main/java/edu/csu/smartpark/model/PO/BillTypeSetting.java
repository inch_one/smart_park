package edu.csu.smartpark.model.PO;

import edu.csu.smartpark.model.DO.BillTypeDO;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "bill_type_settings")
public class BillTypeSetting {
    @Id
    private String id;
    private String parkId;
    private List<BillTypeDO> billTypes;
}
