package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

@TableName("room_info")
@Data
public class RoomPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String buildingNumber;
    private String floorNumber;
    private String roomNumber;
    private int type;
    private double roomSize;
    private int roomCapacity;
    private String remark;
    private String parkId;
    private int status;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
}
