package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import java.util.Date;

@Data
@TableName("article_info")
public class ArticleInfoPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private int type;
    private String title;
    private String articleId;
    private String coverImageUrl;
    private Date releaseTime;
    private String parkId;
    private String publisherId;
    private int viewNum;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
