package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@TableName("enterprise_room")
@Data
public class EnterpriseRoomPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String enterpriseId;
    private String room_id;
    private String usage;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
