package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("work_order")
public class WorkOrderPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String applicantName;
    private String applicantPhone;
    private String applicantId;
    private String workOrderSpecific;
    private Integer typeCode;
    private String parkId;
    private Integer state;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
}
