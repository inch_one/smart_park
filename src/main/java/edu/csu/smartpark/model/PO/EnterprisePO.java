package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("enterprise_info")
public class EnterprisePO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String code;
    private String name;
    private String logoUrl;
    private String briefIntroduction;
    private String picture;
    private String phone;
    private String introduction;
    private int type;
    private String address;
    private Double longitude;
    private Double latitude;
    private String legalPersonName;
    private String establishmentDate;
    private String businessLicenseUrl;
    private String businessScope;
    private String legalPersonCertificateUrl;
    private String identityNumber;
    private String parkId;
    private Date entryTime;
    private int status;
    private String creatorId;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
