package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("toll_month_card_order")
public class TollMonthCardOrderPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String license;
    private Integer orderType;
    private Integer availableCarType;
    private Integer availableTime;
    private Double orderTotal;
    private Double discountAmount;
    private Double payableAmount;
    private Date payTime;
    private Date oldIndate;
    private Date newIndate;
    private String monthCardProductId;
    private String tollMonthCardId;
    private String parkId;
    private String buyerId;
    private Integer status;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
