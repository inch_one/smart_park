package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("parking_order")
public class ParkingOrderPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String inoutId;
    private String license;
    private Double orderTotal;
    private Double discountAmount;
    private Double payableAmount;
    private Date payTime;
    private String parkId;
    private Integer status;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @Version
    private Integer version; // mybatis-plus使用版本号实现乐观锁
}
