package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/*
* @Description: 停车场收费界面跳转二维码
* @Author: LZY
* @Date: 2021/6/24 15:49
*/
@Data
@TableName("parking_qr_code")
public class ParkingQRCodePO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String spaceId;
    private String deviceId;
    private String imageStr;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
