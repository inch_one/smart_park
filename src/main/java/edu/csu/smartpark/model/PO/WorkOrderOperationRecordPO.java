package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@TableName("work_order_operation_record")
@Data
public class WorkOrderOperationRecordPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String workOrderId;
    private Integer operationType;
    private String feedback;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
}
