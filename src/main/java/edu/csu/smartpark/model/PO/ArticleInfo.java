package edu.csu.smartpark.model.PO;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/*
* @Description: 富文本对象，文章类型包含公司简介、园区简介等
* @Author: LZY
* @Date: 2021/4/21 20:57
*/

@Document(collection = "articles")
@Data
public class ArticleInfo {
    private String articleId;
    private String text;
    private List<String> imageUrls;
}
