package edu.csu.smartpark.model.PO;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("toll_month_card")
public class TollMonthCardPO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;
    private String cardName;
    private String license;
    private String briefIntroduction;
    private Integer availableCarType;
    private Integer availableTime;
    private Date dueTime;
    private String parkId;
    private String buyerId;
    private Integer status;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
