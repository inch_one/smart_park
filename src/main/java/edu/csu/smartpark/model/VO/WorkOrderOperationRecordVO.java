package edu.csu.smartpark.model.VO;

import lombok.Data;

import java.util.Date;

@Data
public class WorkOrderOperationRecordVO {
    private String id;
    private String workOrderId;
    private Integer operationType;
    private String feedback;
    private Date updateTime;
}
