package edu.csu.smartpark.model.VO;

import lombok.Data;

@Data
public class ParkingQRCodeVO {
    private String id;
    private String spaceId;
    private String deviceId;
    private String imageStr;
}
