package edu.csu.smartpark.model.VO;

import lombok.Data;

@Data
public class ParkSpaceVO {
    private String spaceId;
    private String name;
    private String parentId;
    private String typeCode;
    private String path;
    private Integer direction;
    private Integer roomSize;
    private Integer roomCapacity;
}
