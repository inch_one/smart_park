package edu.csu.smartpark.model.VO;

import edu.csu.smartpark.model.DO.BaseDO.BaseWorkOrderSpecific;
import lombok.Data;

@Data
public class WorkOrderVO {
    private String id;
    private String applicantName;
    private String applicantPhone;
    private String applicantId;
    private BaseWorkOrderSpecific workOrderSpecific;
    private Integer typeCode;
    private String parkId;
    private Integer state;
}
