package edu.csu.smartpark.model.VO;

import lombok.Data;

@Data
public class LicensePlateResultVO {
    private String info;
    private String plateId;
    private String isPay;
//    private Integer port;
//    private String snapImageRelativeUrl;
//    private String snapImageAbsolutelyUrl;
}
