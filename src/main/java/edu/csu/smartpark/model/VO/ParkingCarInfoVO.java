package edu.csu.smartpark.model.VO;

import lombok.Data;

import java.util.Date;

@Data
public class ParkingCarInfoVO {
    private String orderId;
    private Double orderTotal;
    private Double discountAmount;
    private Double payableAmount;
    private String license;
    private Date entryTime;
    private Date outTime;
}
