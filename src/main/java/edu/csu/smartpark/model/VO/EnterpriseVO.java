package edu.csu.smartpark.model.VO;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EnterpriseVO {
    private String id;
    private String code;
    private String name;
    private String logoUrl;
    private String briefIntroduction;
    private List<String> pictureUrls;
    private String phone;
    private String introduction;
    private int type;
    private String address;
    private Double longitude;
    private Double latitude;
    private String parkId;
    private Date entryTime;
    private int status;
    private Date createTime;
    private Date updateTime;
}
