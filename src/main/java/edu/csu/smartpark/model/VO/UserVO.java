package edu.csu.smartpark.model.VO;

import lombok.Data;

import java.util.List;

@Data
public class UserVO {
    private String id;
    private String nickname;
    private String realName;
    private String phone;
    private String email;
    private String avatarUrl;
    private int age;
    private int gender;
    private List<String> roles;
    private String enterpriseId;
    private String parkId;
    private int status;

}
