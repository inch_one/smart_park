package edu.csu.smartpark.model.VO;

import lombok.Data;

import java.util.List;

/**
 * 类名称：PageVO
 * 类描述：TODO
 * 创建人：CSUpipixia
 * 创建时间：2021/5/2 17:25
 *
 * @version v1.0
 */
@Data
public class PageVO<T> {
    long total;
    long pageSize;
    long pageNum;
    long pages;
    List<T> records;
}
