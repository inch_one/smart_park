package edu.csu.smartpark.model.VO;

import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class ArticleVO {
    private int type;
    private String title;
    private String articleId;
    private List<String> coverImageUrl;
    private Date releaseTime;
    private String parkId;
    private String publisherId;
    private int viewNum;
    private List<String> imageUrls;
    private String text;
}
