package edu.csu.smartpark.model.VO;

import edu.csu.smartpark.model.DO.BillTypeDO;
import lombok.Data;

import java.util.List;

@Data
public class BillTypeSettingVO {
    private String parkId;
    List<BillTypeDO> billTypes;
}
