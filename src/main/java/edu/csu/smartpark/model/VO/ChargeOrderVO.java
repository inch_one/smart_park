package edu.csu.smartpark.model.VO;

import lombok.Data;
import org.springframework.scheduling.support.SimpleTriggerContext;

import javax.swing.*;

/**
 * @author SHW
 * @date 2021/7/14 1:21
 */
@Data
public class ChargeOrderVO {
    //toll_month_card_order
    private String orderId;
    private Double orderTotal;
    private String paymentTime;
    private String parkId;
    private Integer status;
    private Double payableAmount;
    private Double discountAmount;
    //parking_order
    private String license;
    private String inoutId;




}
