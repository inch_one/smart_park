package edu.csu.smartpark.model.VO;

import lombok.Data;

/**
 * @author SHW
 * @date 2021/7/14 23:36
 */
@Data
public class TollIotDetailVO {
    private String deviceId;
    private String deviceName;
    private String id;
    private String spaceId;
    private String parkId;
    private String parkingAreaId;
    private String spaceName;
}
