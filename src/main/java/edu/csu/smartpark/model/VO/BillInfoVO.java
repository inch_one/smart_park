package edu.csu.smartpark.model.VO;

import edu.csu.smartpark.model.DO.BillContentDO;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BillInfoVO {
    private String id;
    private String billNumber;
    private String parkId;
    private String enterpriseId;
    private List<BillContentDO> billContents;
    private double payableTotal;
    private double reducedTotal;
    private double actualTotal;
    private String remark;
    private Date chargeDate; // 收费日期
    private String paymentDocumentUrl; // 支付凭证截图url
}
