package edu.csu.smartpark.service;

import edu.csu.smartpark.model.DO.WorkOrderDO;
import edu.csu.smartpark.model.DO.WorkOrderOperationRecordDO;
import edu.csu.smartpark.model.DTO.WorkOrderOperationRecordQueryDTO;
import edu.csu.smartpark.model.DTO.WorkOrderQueryDTO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.VO.WorkOrderVO;
import edu.csu.smartpark.model.common.BusinessException;

import java.util.List;

public interface WorkOrderService {
    String createWorkOrder(WorkOrderDO workOrderDO) throws BusinessException;
    int updateWorkOrder(WorkOrderDO workOrderDO) throws BusinessException;
    int deleteWorkOrder(String id) throws BusinessException;
    PageVO<WorkOrderVO> queryWorkOrder(WorkOrderQueryDTO workOrderQueryDTO) throws BusinessException;
    String createWorkOrderOperationRecord(WorkOrderOperationRecordDO workOrderOperationRecordDO) throws BusinessException;
    List<WorkOrderOperationRecordDO> getWorkOrderOperationRecords(WorkOrderOperationRecordQueryDTO workOrderOperationRecordQueryDTO) throws BusinessException;
}
