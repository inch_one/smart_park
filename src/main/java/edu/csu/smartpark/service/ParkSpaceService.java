package edu.csu.smartpark.service;


import edu.csu.smartpark.model.DO.ParkSpaceDO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.common.BusinessException;

import java.util.List;
import java.util.Map;

public interface ParkSpaceService {
    String createParkSpace(ParkSpaceDO parkSpaceDO) throws BusinessException;
    int deleteParkSpace(String spaceId) throws BusinessException;
    int updateParkSpace(ParkSpaceDO parkSpaceDO);
    PageVO<ParkSpaceDO> getSubSpaceList(int pageNum, int pageSize, String spaceId, String typeCode) throws BusinessException;
    Map<String, ParkSpaceDO> getSpaceInfos(List<String> spaceIds) throws BusinessException;
    List<ParkSpaceDO> getSpaceInfoByName(String spaceName) throws BusinessException;
    boolean isSpaceNameExist(String spaceId, String name, String parentId) throws BusinessException;
}
