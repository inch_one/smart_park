package edu.csu.smartpark.service;

import com.obs.services.model.PutObjectResult;
import edu.csu.smartpark.model.DO.*;
import edu.csu.smartpark.model.DTO.ChargeOrderDTO;
import edu.csu.smartpark.model.DTO.TollMonthCardProductDTO;
import edu.csu.smartpark.model.DTO.TollMonthCardProductQueryDTO;
import edu.csu.smartpark.model.PO.ParkingRecordPO;
import edu.csu.smartpark.model.PO.TollMonthCardOrderPO;
import edu.csu.smartpark.model.VO.ChargeOrderVO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.VO.TollIotDetailVO;
import edu.csu.smartpark.model.common.BusinessException;

import java.util.Date;
import java.util.List;

public interface ParkingService {
    void bingParkingDevice(String deviceId, String spaceId) throws  BusinessException;
    void unbindParkingDevice(String deviceId, String spaceId) throws  BusinessException;
    //List<ParkingRecordPO> getParkingRecord();
    //List<>
    //实现4.  分页擦操作查询月卡
    PageVO<TollMonthCardDO> getTollMonthCardList(int pageNum, int pageSize, TollMonthCardQueryDO tollMonthCardQueryDO) throws BusinessException;
    //实现5. 月卡商品创建接口
    String creatTollMonthProduct(TollMonthCardProductDO tollMonthCardProductDO) throws BusinessException;
    //实现6. 月卡商品修改接口
    int updateTollMonthProduct(TollMonthCardProductDO tollMonthCardProductDO);
    //实现7. 月卡商品查询接口
    PageVO<TollMonthCardProductDO> getTollMonthCardProductList(int pageNum, int pageSize, TollMonthCardProductQueryDTO tollMonthCardProductQueryDTO) throws BusinessException;
    //删除月卡
    int removeMonthCardProduct(String productId);
    //查询停车收费订单订单
    PageVO<ChargeOrderVO>getChargeOrderList(ChargeOrderDTO chargeOrderDTO);
    //查询月卡充值订单
    PageVO<TollMonthCardOrderPO>getTollMonthCardOrderPOList(String id,int pageSize,int pageNum);
    //创建设备
    String createDevice(String deviceId,String deviceName);
    //更新数据
    void updateDevice(String deviceId,String deviceName);
    //删除设备
    int removeDevice(String id);
    //查询设备（分页）
    PageVO<TollIotDetailVO>getDeviceList(int pageSize,int pageNum);
    List<ParkingRecordPO> getParkingRecordQueryDO(ParkingRecordQueryDO parkingRecordQueryDO) throws BusinessException;
    TollIotDetailDO getTollIotDetailDO(String deviceId) throws BusinessException;
    boolean isAllowedEntry(String license) throws BusinessException;
    String createParkingRecord(ParkingRecordDO parkingRecordDO) throws BusinessException;
    void updateParkingRecord(ParkingRecordDO parkingRecordDO) throws BusinessException;
    List<ParkingRecordDO> getParkingRecord(ParkingRecordQueryDO parkingRecordQueryDO) throws BusinessException;
    double calculateParkingFee(String tollStandardId, Date entryTime, Date outTime) throws  BusinessException;
    boolean isTollMonthCardValid(TollMonthCardDO tollMonthCardDO, ParkingRecordDO parkingRecordDO) throws BusinessException;
    //    PutObjectResult receiveSnapImage(String image) throws BusinessException;
    ParkingQRCodeDO generateParkingQRCode(String content, String spaceId, String deviceId) throws BusinessException;
    ParkingQRCodeDO getParkingQRCode(String spaceId) throws BusinessException;
    void deleteParkingQRCode(String spaceId) throws BusinessException;
    void createTollMonthCard(TollMonthCardDO tollMonthCardDO) throws BusinessException;
    List<TollMonthCardDO> getTollMonthCard(TollMonthCardQueryDO tollMonthCardQueryDO) throws BusinessException;
    void updateTollMonthCard(TollMonthCardDO tollMonthCardDO) throws BusinessException;
}
