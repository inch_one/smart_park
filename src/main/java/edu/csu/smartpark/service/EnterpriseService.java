package edu.csu.smartpark.service;


import edu.csu.smartpark.model.DO.EnterpriseDO;
import edu.csu.smartpark.model.DTO.EnterpriseBasicDTO;
import edu.csu.smartpark.model.DTO.EnterpriseDTO;
import edu.csu.smartpark.model.DTO.EnterpriseQueryDTO;
import edu.csu.smartpark.model.PO.EnterprisePO;
import edu.csu.smartpark.model.VO.EnterpriseVO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.common.BusinessException;

import java.util.List;

public interface EnterpriseService {
    List<EnterpriseDO> getAllEnterprise(String enterpriseName);
    EnterpriseDO getEnterpriseInfoById(String enterpriseId);
    List<EnterprisePO> getEnterpriseInfoByCreatorId(String creatorId);
    int updateEnterpriseInfo(EnterpriseBasicDTO enterpriseBasicDTO);
    String createEnterprise(EnterpriseDTO enterpriseDTO, String creatorId);
    int checkEnterpriseCode(String enterpriseCode);
    int updateEnterpriseStatus(String enterpriseId, int status);
    PageVO<EnterpriseVO> queryEnterprise(EnterpriseQueryDTO enterpriseQueryDTO) throws BusinessException;
}
