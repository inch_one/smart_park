package edu.csu.smartpark.service;

import edu.csu.smartpark.model.DO.*;
import edu.csu.smartpark.model.common.BusinessException;
import java.util.List;

public interface OrderService {
    String createParkingOrder(ParkingOrderDO parkingOrderDO) throws BusinessException;
    void updateParkingOrder(ParkingOrderDO parkingOrderDO) throws BusinessException;
    List<ParkingOrderDO> getParkingOrder(ParkingOrderQueryDO parkingOrderQueryDO) throws BusinessException;
    PayOrderCommitResultDO commitParkingPayOrder(ParkingOrderDO parkingOrderDO) throws BusinessException;
    boolean isParkingOrderPay(String orderId) throws BusinessException;
    String createMonthCardOrder(TollMonthCardOrderDO tollMonthCardOrderDO) throws BusinessException;
    void updateMonthCardOrder(TollMonthCardOrderDO tollMonthCardOrderDO) throws BusinessException;
    List<TollMonthCardOrderDO> getMonthCardOrder(TollMonthCardOrderQueryDO tollMonthCardOrderQueryDO) throws BusinessException;
    PayOrderCommitResultDO commitMonthCardPayOrder(TollMonthCardOrderDO tollMonthCardOrderDO) throws BusinessException;
}
