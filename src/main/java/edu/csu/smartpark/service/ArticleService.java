package edu.csu.smartpark.service;

import edu.csu.smartpark.model.DO.ArticleDO;
import edu.csu.smartpark.model.DTO.ArticleQueryDTO;
import edu.csu.smartpark.model.VO.ArticleVO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.common.BusinessException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface ArticleService {
    String createArticle(ArticleDO articleDO);
    PageVO<ArticleVO> queryArticle(ArticleQueryDTO queryDTO) throws BusinessException;
    ArticleDO getArticleInfoByArticleId(String articleId);
    int updateArticleInfo(ArticleDO articleDO);
    int deleteArticle(String articleId);
    List<String> uploadArticleImages(MultipartFile[] files);
}
