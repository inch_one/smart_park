package edu.csu.smartpark.service;

import edu.csu.smartpark.model.DO.BillTypeDO;
import edu.csu.smartpark.model.DTO.BillDTO;
import edu.csu.smartpark.model.DTO.BillQueryDTO;
import edu.csu.smartpark.model.PO.BillTypeSetting;

import java.util.List;
import java.util.Map;

public interface BillService {
    public String addBill(BillDTO billDTO);
    public int removeBill(String billId);
    public Map<String, Object> getBillList(BillQueryDTO billQueryDTO);
    public int addBillType(String parkId, String name);
    public int editBillType(String parkId, String typeId, String newTypeName);
    public int deleteBillType(String parkId, String typeId);
    public BillTypeSetting getBillTypes(String parkId);
}
