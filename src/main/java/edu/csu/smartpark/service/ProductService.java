package edu.csu.smartpark.service;

import edu.csu.smartpark.model.DO.TollMonthCardProductDO;
import edu.csu.smartpark.model.DO.TollMonthCardProductQueryDO;
import edu.csu.smartpark.model.common.BusinessException;

import java.util.List;

public interface ProductService {
    String createTollMonthCardProduct(TollMonthCardProductDO tollMonthCardProductDO) throws BusinessException;
    void updateTollMonthCardProduct(TollMonthCardProductDO tollMonthCardProductDO) throws BusinessException;
    List<TollMonthCardProductDO> getTollMonthCardProduct(TollMonthCardProductQueryDO tollMonthCardProductQueryDO) throws BusinessException;
    void deleteTollMonthCardProduct(TollMonthCardProductDO tollMonthCardProductDO) throws BusinessException;

}
