package edu.csu.smartpark.service;

import edu.csu.smartpark.model.DO.UserDO;
import edu.csu.smartpark.model.common.BusinessException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {
    UserDO getUserById(String id);
    UserDO getUserByPhone(String phone);
    boolean isPhoneExist(String phone);
    String register(UserDO userDO);
    String uploadImage(String phone, MultipartFile file);
    int updatePassword(String phone, String password);
    int setCompanyStaff(String donorPhone, String authorizedPhone,String companyId);
    int setFirstLevelOwnerManager(String donorPhone, String authorizedPhone, String companyId);
    int setSecondLevelOwnerManager(String donorPhone, String authorizedPhone, String companyId);
    int updateUser(UserDO userDO);
    int updatePhone(String userId, String newPhone);
    int updateEmail(String phone, String email);
    void setEnterpriseUserRole(String userId,String authorizedPhone, String authorizeRole) throws BusinessException;
    int updateEnterpriseOfUser(String userId, String enterpriseId);
}