package edu.csu.smartpark.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.csu.smartpark.model.DO.ParkDO;
import edu.csu.smartpark.model.PO.ParkPO;
import edu.csu.smartpark.model.VO.PageVO;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface ParkService {
    List<ParkPO> getAllParks();
    PageVO<ParkDO> queryParksForPage(long pageSize, long pageNum) throws InvocationTargetException, IllegalAccessException;
    ParkDO getParkInfoById(String parkId);
    int updateParkInfo(ParkDO parkDO);
    int createPark(ParkDO parkDO);
    int deletePark(String parkId);
}
