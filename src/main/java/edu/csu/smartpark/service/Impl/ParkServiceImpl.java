package edu.csu.smartpark.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.csu.smartpark.dao.ParkDAO;
import edu.csu.smartpark.model.DO.ParkDO;
import edu.csu.smartpark.model.PO.ParkPO;
import edu.csu.smartpark.model.PO.UserPO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.service.ParkService;
import edu.csu.smartpark.util.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ParkServiceImpl implements ParkService {

    @Autowired
    private ParkDAO parkDAO;

    @Override
    public List<ParkPO> getAllParks() {
        QueryWrapper<ParkPO> warpper = new QueryWrapper<>();
        warpper.select("id", "park_name", "province", "city", "area");

        List<ParkPO> list = parkDAO.selectList(warpper);
        if (list.size() != 0) {
            return list;
        }
        return null;
    }

    @Override
    public PageVO<ParkDO> queryParksForPage(long pageSize, long pageNum) throws InvocationTargetException, IllegalAccessException {

        IPage<ParkPO> parkPOIPage = new Page<>(pageNum, pageSize);

        parkPOIPage = parkDAO.selectPage(parkPOIPage, null);

        List<ParkDO> list = new ArrayList<>();
        for (ParkPO parkPO : parkPOIPage.getRecords()) {
            if (parkPO != null) {
                ParkDO parkDO = new ParkDO();
                BeanUtils.copyProperties(parkDO, parkPO);
                List<String> parkImageUrlList = StringUtil.splits(parkPO.getParkImageUrl());
                parkDO.setParkImageUrlList(parkImageUrlList);
                list.add(parkDO);
            }
        }

        PageVO<ParkDO> pageVO = new PageVO<>();
        pageVO.setTotal(parkPOIPage.getTotal());
        pageVO.setPageSize(list.size());
        pageVO.setPageNum(parkPOIPage.getCurrent());
        pageVO.setPages(parkPOIPage.getPages());
        pageVO.setRecords(list);
        return pageVO;
    }

    @Override
    public ParkDO getParkInfoById(String parkId) {
        ParkPO parkPO = parkDAO.selectById(parkId);
        if (parkPO != null) {
            ParkDO parkDO = new ParkDO();
            BeanUtils.copyProperties(parkPO, parkDO);
            List<String> parkImageUrlList = StringUtil.splits(parkPO.getParkImageUrl());
            parkDO.setParkImageUrlList(parkImageUrlList);
            return parkDO;
        }
        return null;
    }

    @Override
    public int updateParkInfo(ParkDO parkDO) {
        ParkPO parkPO = new ParkPO();
        parkPO.setParkName(parkDO.getParkName());
        parkPO.setBriefIntroduction(parkDO.getBriefIntroduction());
        parkPO.setContactDetails(parkDO.getContactDetails());
        parkPO.setAddress(parkDO.getAddress());
        parkPO.setLongitude(parkDO.getLongitude());
        parkPO.setLatitude(parkDO.getLatitude());
        UpdateWrapper<ParkPO> wrapper = new UpdateWrapper<>();
        wrapper.eq("id", parkDO.getId());

        return parkDAO.update(parkPO, wrapper);
    }

    @Override
    public int createPark(ParkDO parkDO) {
        return 0;
    }

    @Override
    public int deletePark(String parkId) {
        return 0;
    }
}
