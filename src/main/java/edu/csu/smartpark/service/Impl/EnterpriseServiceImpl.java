package edu.csu.smartpark.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.csu.smartpark.dao.EnterpriseDAO;
import edu.csu.smartpark.model.DO.EnterpriseDO;
import edu.csu.smartpark.model.DTO.EnterpriseBasicDTO;
import edu.csu.smartpark.model.DTO.EnterpriseDTO;
import edu.csu.smartpark.model.DTO.EnterpriseQueryDTO;
import edu.csu.smartpark.model.PO.EnterprisePO;
import edu.csu.smartpark.model.VO.EnterpriseVO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.common.BusinessException;
import edu.csu.smartpark.service.EnterpriseService;
import edu.csu.smartpark.util.OssUtil;
import edu.csu.smartpark.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
@Slf4j
public class EnterpriseServiceImpl implements EnterpriseService {

    @Autowired
    private EnterpriseDAO enterpriseDAO;

    @Autowired
    private OssUtil ossUtil;

    @Override
    public List<EnterpriseDO> getAllEnterprise(String enterpriseName) {
        QueryWrapper<EnterprisePO> wrapper = new QueryWrapper<>();
        wrapper.like("name", enterpriseName)
                .eq("status", 1)
                .eq("is_deleted", 0);
        List<EnterprisePO> enterprisePOList = enterpriseDAO.selectList(wrapper);
        if (enterprisePOList.size() != 0) {
            List<EnterpriseDO> enterpriseDOList = new ArrayList<>();
            for(EnterprisePO enterprisePO : enterprisePOList){
                EnterpriseDO enterpriseDO = new EnterpriseDO();
                BeanUtils.copyProperties(enterprisePO, enterpriseDO);
                enterpriseDO.setPictureUrls(StringUtil.splits(enterprisePO.getPicture()));
                enterpriseDOList.add(enterpriseDO);
            }

            return enterpriseDOList;
        }
        return null;
    }

    @Override
    public EnterpriseDO getEnterpriseInfoById(String enterpriseId) {
        EnterprisePO enterprisePO = enterpriseDAO.selectById(enterpriseId);
        if (enterprisePO != null) {
            EnterpriseDO enterpriseDO = new EnterpriseDO();
            BeanUtils.copyProperties(enterprisePO, enterpriseDO);
            enterpriseDO.setPictureUrls(StringUtil.splits(enterprisePO.getPicture()));
            return enterpriseDO;
        }
        return null;
    }

    @Override
    public List<EnterprisePO> getEnterpriseInfoByCreatorId(String creatorId) {
        QueryWrapper<EnterprisePO> wrapper = new QueryWrapper<>();
        wrapper.eq("creator_id", creatorId);
        return enterpriseDAO.selectList(wrapper);
    }

    @Override
    public int updateEnterpriseInfo(EnterpriseBasicDTO enterpriseBasicDTO) {
        UpdateWrapper<EnterprisePO> updateWrapper = new UpdateWrapper<>();

        updateWrapper.eq("id", enterpriseBasicDTO.getId())
                .set("name", enterpriseBasicDTO.getName())
                .set("brief_introduction", enterpriseBasicDTO.getBriefIntroduction())
                .set("phone", enterpriseBasicDTO.getPhone())
                .set("address", enterpriseBasicDTO.getAddress())
                .set("longitude", enterpriseBasicDTO.getLongitude())
                .set("latitude", enterpriseBasicDTO.getLatitude())
                .set("entry_time", enterpriseBasicDTO.getEntryTime());

        return enterpriseDAO.update(null, updateWrapper);
    }

    @Override
    public String createEnterprise(EnterpriseDTO enterpriseDTO, String creatorId) {
        EnterprisePO enterprisePO = new EnterprisePO();
        BeanUtils.copyProperties(enterpriseDTO, enterprisePO);

        // 上传logo
        String logoUrl = ossUtil.uploadImage(enterpriseDTO.getLogo()).getObjectUrl();
        // 上传企业证书
        String businessLicenseUrl = ossUtil.uploadImage(enterpriseDTO.getBusinessLicense()).getObjectUrl();
        // 上传法人证件
        String legalPersonCertificateUrl = ossUtil.uploadImage(enterpriseDTO.getLegalPersonCertificate()).getObjectUrl();

        enterprisePO.setLogoUrl(logoUrl);
        enterprisePO.setBusinessLicenseUrl(businessLicenseUrl);
        enterprisePO.setLegalPersonCertificateUrl(legalPersonCertificateUrl);


        // 创建企业必须经过审核，-1-驳回，0-待审核，1-正常，2-冻结
        enterprisePO.setStatus(0);
        enterprisePO.setCreatorId(creatorId);

        int result = enterpriseDAO.insert(enterprisePO);

        if (result > 0) {
            return enterprisePO.getId();
        }

        return "";
    }

    @Override
    public int checkEnterpriseCode(String enterpriseCode) {
        QueryWrapper<EnterprisePO> wrapper = new QueryWrapper<>();
        wrapper.eq("code", enterpriseCode);
        return enterpriseDAO.selectList(wrapper).size();
    }

    @Override
    public int updateEnterpriseStatus(String enterpriseId, int status) {
        UpdateWrapper<EnterprisePO> wrapper = new UpdateWrapper<>();
        wrapper.eq("id", enterpriseId).set("status", status);
        return enterpriseDAO.update(null, wrapper);
    }

    @Override
    public PageVO<EnterpriseVO> queryEnterprise(EnterpriseQueryDTO enterpriseQueryDTO) throws BusinessException {
        if (enterpriseQueryDTO == null) {
            throw new BusinessException(BusinessException.PARAMETERERROR, "enterprise query dto is null");
        }
        QueryWrapper<EnterprisePO> wrapper = new QueryWrapper<>();
        if (!StringUtil.isEmpty(enterpriseQueryDTO.getName())){
            wrapper.like("name", enterpriseQueryDTO.getName());
        }
        if (!StringUtil.isEmpty(enterpriseQueryDTO.getCode())){
            wrapper.eq("code", enterpriseQueryDTO.getCode());
        }
        if (enterpriseQueryDTO.getType() != null){
            wrapper.eq("type", enterpriseQueryDTO.getType());
        }
        if (!StringUtil.isEmpty(enterpriseQueryDTO.getParkId())){
            wrapper.eq("park_id", enterpriseQueryDTO.getParkId());
        }
        if (enterpriseQueryDTO.getStatus() != null){
            wrapper.eq("status", enterpriseQueryDTO.getStatus());
        }
        Integer pageNum = enterpriseQueryDTO.getPageNum() == null ? 1 : enterpriseQueryDTO.getPageNum();
        Integer pageSize = enterpriseQueryDTO.getPageSize() == null ? 10 : enterpriseQueryDTO.getPageSize();
        Page<EnterprisePO> page = new Page<>(pageNum, pageSize);
        IPage<EnterprisePO> iPage = enterpriseDAO.selectPage(page, wrapper);
        List<EnterpriseVO> enterpriseVOS = new LinkedList<>();
        for (EnterprisePO enterprisePO : iPage.getRecords()){
            EnterpriseVO enterpriseVO = new EnterpriseVO();
            BeanUtils.copyProperties(enterprisePO, enterpriseVO);
            enterpriseVOS.add(enterpriseVO);
        }
        PageVO<EnterpriseVO> res = new PageVO<>();
        res.setPageNum(iPage.getCurrent());
        res.setPageSize(iPage.getSize());
        res.setPages(iPage.getPages());
        res.setTotal(iPage.getTotal());
        res.setRecords(enterpriseVOS);
        return res;
    }
}
