package edu.csu.smartpark.service.Impl;

import edu.csu.smartpark.model.DO.TollMonthCardProductDO;
import edu.csu.smartpark.model.DO.TollMonthCardProductQueryDO;
import edu.csu.smartpark.model.common.BusinessException;
import edu.csu.smartpark.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Override
    public String createTollMonthCardProduct(TollMonthCardProductDO tollMonthCardProductDO) throws BusinessException {
        return null;
    }

    @Override
    public void updateTollMonthCardProduct(TollMonthCardProductDO tollMonthCardProductDO) throws BusinessException {

    }

    @Override
    public List<TollMonthCardProductDO> getTollMonthCardProduct(TollMonthCardProductQueryDO tollMonthCardProductQueryDO) throws BusinessException {
        return null;
    }

    @Override
    public void deleteTollMonthCardProduct(TollMonthCardProductDO tollMonthCardProductDO) throws BusinessException {

    }
}
