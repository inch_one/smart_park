package edu.csu.smartpark.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.obs.services.model.PutObjectResult;
import edu.csu.smartpark.dao.*;
import edu.csu.smartpark.model.DO.*;
import edu.csu.smartpark.model.DTO.ChargeOrderDTO;
import edu.csu.smartpark.model.DTO.TollMonthCardProductDTO;
import edu.csu.smartpark.model.DTO.TollMonthCardProductQueryDTO;
import edu.csu.smartpark.model.PO.*;
import edu.csu.smartpark.model.VO.ChargeOrderVO;
import edu.csu.smartpark.model.VO.PageVO;
import edu.csu.smartpark.model.VO.TollIotDetailVO;
import edu.csu.smartpark.model.common.BusinessException;
import edu.csu.smartpark.service.ParkingService;
import edu.csu.smartpark.util.CommonUtil;
import edu.csu.smartpark.util.QRCodeUtil;
import edu.csu.smartpark.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
@Slf4j
public class ParkingServiceImpl implements ParkingService {

    @Autowired
    private ParkingQRCodeDAO parkingQRCodeDAO;

    @Autowired
    private TollIotDetailDAO tollIotDetailDAO;

    @Autowired
    private ParkingRecordDAO parkingRecordDAO;

    @Autowired
    private TollMonthCardDAO tollMonthCardDAO;

    @Autowired
    private TollMonthCardProductDAO tollMonthCardProductDAO;

    @Autowired
    private TollMonthCardOrderDAO tollMonthCardOrderDAO;




    @Override
    /*
    * @Description: 将车行设备与车道空间进行绑定
    * @Author: LZY
    * @Date: 2021/6/27 11:29
    * @Params: [deviceId, spaceId]
    * @Return: void
    */
    public void bingParkingDevice(String deviceId, String spaceId) throws BusinessException {

    }

    @Override
    /*
    * @Description: 将车行设备与车道空间进行解绑
    * @Author: LZY
    * @Date: 2021/6/27 11:29
    * @Params: [deviceId, spaceId]
    * @Return: void
    */
    public void unbindParkingDevice(String deviceId, String spaceId) throws BusinessException {
        UpdateWrapper<ParkingQRCodePO> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("space_id", spaceId).set("is_deleted", 1);
        parkingQRCodeDAO.update(null, updateWrapper);
    }


    public List<ParkingRecordPO> getParkingRecordQueryDO(ParkingRecordQueryDO parkingRecordQueryDO) throws BusinessException {
        if (parkingRecordQueryDO == null){
            log.warn("[ParkingService] query parking record fail, because query condition is null");
            return new LinkedList<>();
        }

        QueryWrapper<ParkingRecordPO> queryWrapper = new QueryWrapper<>();
        String id = parkingRecordQueryDO.getId();
        queryWrapper.eq(StringUtil.isNotEmpty(id), "id", id);
        String license = parkingRecordQueryDO.getLicense();
        queryWrapper.eq(StringUtil.isNotEmpty(license), "license", license);
        String parkId = parkingRecordQueryDO.getParkId();
        queryWrapper.eq(StringUtil.isNotEmpty(parkId), "park_id", parkId);
        Integer vehicleState = parkingRecordQueryDO.getVehicleState();
        queryWrapper.eq(vehicleState != null, "vehicle_state", vehicleState);
        Integer vehicleType = parkingRecordQueryDO.getVehicleType();
        queryWrapper.eq(vehicleType != null, "vehicle_type", vehicleType);
        Integer status = parkingRecordQueryDO.getStatus();
        queryWrapper.eq(status != null, "status", status);
        Date startTime = parkingRecordQueryDO.getStartTime();
        queryWrapper.ge(startTime != null, "out_time", startTime);
        Date endTime = parkingRecordQueryDO.getEndTime();
        queryWrapper.le(endTime != null, "out_time", endTime);
        List<ParkingRecordPO> parkingRecordPOS = parkingRecordDAO.selectList(queryWrapper);

        return parkingRecordPOS;
    }

    /*
     * @Description: 通过userid查找月卡记录
     * @Author: SHW
     * @Date: 2021/7/9 9：23
     * @Params:
     * @Return: PageVO<TollMonthCardDO>
     */
    @Override
    public PageVO<TollMonthCardDO>getTollMonthCardList(int pageNum, int pageSize,TollMonthCardQueryDO tollMonthCardQueryDO) throws BusinessException{
//        TollMonthCardQueryDO tollMonthCardQueryDO = new TollMonthCardQueryDO();
//        tollMonthCardQueryDO.setLicense(license);
//        tollMonthCardQueryDO.setId(id);
//        tollMonthCardQueryDO.setParkId(parkId);
//        tollMonthCardQueryDO.setStatus(status);
//        tollMonthCardQueryDO.setBuyerId(userId);
        if (tollMonthCardQueryDO == null){
            log.warn("[ParkingService] query toll month card fail, because query condition is null");
            return new PageVO<>();
        }
        QueryWrapper<TollMonthCardPO> queryWrapper = new QueryWrapper<>();
        String id = tollMonthCardQueryDO.getId();
        queryWrapper.eq(StringUtil.isNotEmpty(id), "id", id);
        String license = tollMonthCardQueryDO.getLicense();
        queryWrapper.eq(StringUtil.isNotEmpty(license), "license", license);
        String buyerId = tollMonthCardQueryDO.getBuyerId();
        queryWrapper.eq(StringUtil.isNotEmpty(buyerId), "buyer_id", buyerId);
        Integer status = tollMonthCardQueryDO.getStatus();
        queryWrapper.eq(status != null, "status", status);
        String parkId = tollMonthCardQueryDO.getParkId();
        queryWrapper.eq(StringUtil.isNotEmpty(parkId), "park_id", parkId);
        List<TollMonthCardPO> tollMonthCardPOS = tollMonthCardDAO.selectList(queryWrapper);

        List<TollMonthCardDO> tollMonthCardDOS = new LinkedList<>();
        for(TollMonthCardPO tollMonthCardPO : tollMonthCardPOS){
            TollMonthCardDO tollMonthCardDO = new TollMonthCardDO();
            BeanUtils.copyProperties(tollMonthCardPO, tollMonthCardDO);
            tollMonthCardDOS.add(tollMonthCardDO);
        }
        Page<TollMonthCardPO> page = new Page<>(pageNum, pageSize);
        IPage<TollMonthCardPO> tollMonthCardPOIPage = tollMonthCardDAO.selectPage(page, queryWrapper);
        PageVO<TollMonthCardDO>pageVO = new PageVO<>();
        pageVO.setTotal(tollMonthCardPOIPage.getTotal());
        pageVO.setPages(tollMonthCardPOIPage.getPages());
        pageVO.setPageNum(tollMonthCardPOIPage.getCurrent());
        pageVO.setPageSize(tollMonthCardDOS.size());
        pageVO.setRecords(tollMonthCardDOS);
        log.info("[mysql] getTollMonthCard of {} list success", buyerId);
        return pageVO;
    }

    /**
     * BY:SHW
     * 2021/7/9
     * @param tollMonthCardProductDO
     * @return
     * @throws BusinessException
     */
    @Override
    public String creatTollMonthProduct(TollMonthCardProductDO tollMonthCardProductDO) throws BusinessException{
        TollMonthCardProductPO tollMonthCardProductPO = new TollMonthCardProductPO();
        BeanUtils.copyProperties(tollMonthCardProductDO,tollMonthCardProductPO);
        int result = tollMonthCardProductDAO.insert(tollMonthCardProductPO);
        String productId = tollMonthCardProductPO.getId();
        return productId;
    }

    /**
     * 修改月卡产品
     * @author SHW
     * @param tollMonthCardProductDO
     * @return
     */
    @Override
    public int updateTollMonthProduct(TollMonthCardProductDO tollMonthCardProductDO){
        QueryWrapper <TollMonthCardProductPO>queryWrapper = new QueryWrapper();
        queryWrapper.eq("id",tollMonthCardProductDO.getId());
        TollMonthCardProductPO tollMonthCardProductPO = tollMonthCardProductDAO.selectOne(queryWrapper);
        BeanUtils.copyProperties(tollMonthCardProductDO,tollMonthCardProductPO);
        if(tollMonthCardProductPO == null){
            log.warn("park space {} is null, update fail", tollMonthCardProductDO.getId());
            throw new BusinessException(BusinessException.PARAMETERERROR, " Month Card Product {} is null, update fail");
        }
        int res = tollMonthCardProductDAO.updateById(tollMonthCardProductPO);
        if(res <= 0){
            log.info("[mysql] update space {} fail", tollMonthCardProductPO.getId());
            throw new BusinessException(BusinessException.SERVERERROR, "update space fail");
        }

        return 1;
    }

    /**查询月卡商品
     * @author SHW
     * @param pageNum
     * @param pageSize
     * @param tollMonthCardProductQueryDTO
     * @return
     * @throws BusinessException
     */
    @Override
    public PageVO<TollMonthCardProductDO> getTollMonthCardProductList(int pageNum, int pageSize, TollMonthCardProductQueryDTO tollMonthCardProductQueryDTO) throws BusinessException{
        QueryWrapper<TollMonthCardProductPO> queryWrapper = new QueryWrapper<>();
        //找到没有删除的月卡商品
        queryWrapper.eq("is_deleted",1);
        String id = tollMonthCardProductQueryDTO.getProductId();
        queryWrapper.eq("id",id);
        Page<TollMonthCardProductPO>page = new Page<>(pageNum,pageSize);
        IPage<TollMonthCardProductPO>tollMonthCardProductPOIPage = tollMonthCardProductDAO.selectPage(page,queryWrapper);
        List<TollMonthCardProductPO> recordPOs = tollMonthCardProductPOIPage.getRecords();
        List<TollMonthCardProductDO> recordDOs = new LinkedList<>();
        for(TollMonthCardProductPO recordPO : recordPOs){
            TollMonthCardProductDO recordDO = new TollMonthCardProductDO();
            BeanUtils.copyProperties(recordPO, recordDO);
            recordDOs.add(recordDO);
        }
        PageVO<TollMonthCardProductDO>pageVO = new PageVO<>();
        pageVO.setPageSize(tollMonthCardProductPOIPage.getSize());
        pageVO.setTotal(tollMonthCardProductPOIPage.getTotal());
        pageVO.setPageNum(tollMonthCardProductPOIPage.getCurrent());
        pageVO.setPages(tollMonthCardProductPOIPage.getPages());
        pageVO.setRecords(recordDOs);
        log.info("[mysql] get TollMonthCardProductList {}",id);
        return pageVO;
    }

    @Override
    public int removeMonthCardProduct(String productId) {
//        UpdateWrapper<TollMonthCardProductPO>  updateWrapper = new UpdateWrapper<>();
//        updateWrapper.eq("id", productId).set("is_deleted", 1);
//        int row = tollMonthCardProductDAO.update(null, updateWrapper);
//        if (row < 0){
//            log.info("[mysql] delete space {} fail", productId);
//            throw new BusinessException(BusinessException.SERVERERROR, "delete space fail");
//        }
//        log.info("[mysql] delete space {} success", productId);
//        return 1;
        QueryWrapper<TollMonthCardProductPO>queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",productId);
        TollMonthCardProductPO tollMonthCardProductPO = tollMonthCardProductDAO.selectOne(queryWrapper);
        if(tollMonthCardProductPO == null){
            log.warn("[mysql] delete not successful");
            throw new BusinessException(BusinessException.SERVERERROR,"FAIL");
        }
        tollMonthCardProductPO.setIsDeleted(1);
        tollMonthCardProductDAO.updateById(tollMonthCardProductPO);
        return 1;
    }

    @Override
    public TollIotDetailDO getTollIotDetailDO(String deviceId) throws BusinessException {
        QueryWrapper<TollIotDetailPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("device_id", deviceId).eq("is_deleted", 0);
        TollIotDetailPO tollIotDetailPO = tollIotDetailDAO.selectOne(queryWrapper);
        if (tollIotDetailPO == null){
            return new TollIotDetailDO();
        }
        TollIotDetailDO tollIotDetailDO = new TollIotDetailDO();
        BeanUtils.copyProperties(tollIotDetailPO, tollIotDetailDO);
        return tollIotDetailDO;
    }

    @Override
    /*
    * @Description: 判断车辆是否拥有进入停车场的权限
    * @Author: LZY
    * @Date: 2021/6/27 11:29
    * @Params: [license]
    * @Return: boolean
    */
    public boolean isAllowedEntry(String license) throws BusinessException {
        return true;
    }

    @Override
    /*
    * @Description: 增添一条车辆进出记录，返回值为记录ID
    * @Author: LZY
    * @Date: 2021/6/27 11:30
    * @Params: [licensePlateResultDO]
    * @Return: java.lang.String
    */
    public String createParkingRecord(ParkingRecordDO parkingRecordDO) throws BusinessException {
        ParkingRecordPO parkingRecordPO = new ParkingRecordPO();
        if (parkingRecordDO == null){
            log.warn("[ParkingService] create parking record fail , because record is null");
            return "";
        }
        BeanUtils.copyProperties(parkingRecordDO, parkingRecordPO);
        int row = parkingRecordDAO.insert(parkingRecordPO);
        return row > 0 ? parkingRecordPO.getId(): "";
    }

    @Override
    /*
    * @Description: 根据车辆进出记录id更新记录，只支持更新单条记录
    * @Author: LZY
    * @Date: 2021/6/28 16:06
    * @Params: [parkingRecordDO]
    * @Return: void
    */
    public void updateParkingRecord(ParkingRecordDO parkingRecordDO) throws BusinessException {
        if (parkingRecordDO == null || parkingRecordDO.getId().isEmpty()){
            log.warn("[ParkingService] update parking record fail, because id or parking record is null");
            return;
        }
        ParkingRecordPO parkingRecordPO = new ParkingRecordPO();
        BeanUtils.copyProperties(parkingRecordDO, parkingRecordPO);
        parkingRecordDAO.updateById(parkingRecordPO);
    }

    @Override
    /*
    * @Description: 根据条件查询符合要求的车辆进出记录，所有的车辆进出记录查询统一走这个接口，根据传入的DO对象中的非null参数构建查询条件
    * @Author: LZY
    * @Date: 2021/6/27 11:31
    * @Params: [parkingRecordDO]
    * @Return: java.util.List<edu.csu.smartpark.model.DO.ParkingRecordDO>
    */
    public List<ParkingRecordDO> getParkingRecord(ParkingRecordQueryDO parkingRecordQueryDO) throws BusinessException {
        if (parkingRecordQueryDO == null){
            log.warn("[ParkingService] query parking record fail, because query condition is null");
            return new LinkedList<>();
        }

        QueryWrapper<ParkingRecordPO> queryWrapper = new QueryWrapper<>();
        String id = parkingRecordQueryDO.getId();
        queryWrapper.eq(StringUtil.isNotEmpty(id), "id", id);
        String license = parkingRecordQueryDO.getLicense();
        queryWrapper.eq(StringUtil.isNotEmpty(license), "license", license);
        String parkId = parkingRecordQueryDO.getParkId();
        queryWrapper.eq(StringUtil.isNotEmpty(parkId), "park_id", parkId);
        Integer vehicleState = parkingRecordQueryDO.getVehicleState();
        queryWrapper.eq(vehicleState != null, "vehicle_state", vehicleState);
        Integer vehicleType = parkingRecordQueryDO.getVehicleType();
        queryWrapper.eq(vehicleType != null, "vehicle_type", vehicleType);
        Integer status = parkingRecordQueryDO.getStatus();
        queryWrapper.eq(status != null, "status", status);
        Date startTime = parkingRecordQueryDO.getStartTime();
        queryWrapper.ge(startTime != null, "out_time", startTime);
        Date endTime = parkingRecordQueryDO.getEndTime();
        queryWrapper.le(endTime != null, "out_time", endTime);
        List<ParkingRecordPO> parkingRecordPOS = parkingRecordDAO.selectList(queryWrapper);

        List<ParkingRecordDO> parkingRecordDOS = new LinkedList<>();
        for (ParkingRecordPO parkingRecordPO : parkingRecordPOS){
            ParkingRecordDO parkingRecordDO = new ParkingRecordDO();
            BeanUtils.copyProperties(parkingRecordPO, parkingRecordDO);
            parkingRecordDOS.add(parkingRecordDO);
        }
        return parkingRecordDOS;
    }

    @Override
    /*
    * @Description: 根据计费规则计算停车收费
    * @Author: LZY
    * @Date: 2021/6/27 11:32
    * @Params: [entryTime, outTime]
    * @Return: double
    */
    public double calculateParkingFee(String tollStandardId, Date entryTime, Date outTime) throws BusinessException {
        // TODO 根据计费标准id加载计费标准，计算费用
        return 0;
    }

    @Override
    /*
    * @Description: 判断是否月卡有效
    * @Author: LZY
    * @Date: 2021/6/27 16:48
    * @Params: [tollMonthCardDO]
    * @Return: boolean
    */
    public boolean isTollMonthCardValid(TollMonthCardDO tollMonthCardDO, ParkingRecordDO parkingRecordDO) throws BusinessException {
        /*月卡有效性的判断逻辑
            1. 月卡是否已经到期
            2. 车型是否准确
            3. 园区是否准确
            TODO 后面要考虑日间、夜间的月卡情况
         */
        boolean isValid = true;
        if (tollMonthCardDO.getDueTime().before(CommonUtil.getCurrentTime())){
            isValid = false;
        }
        if (!tollMonthCardDO.getAvailableCarType().equals(parkingRecordDO.getVehicleType())){
            isValid = false;
        }
        if (!tollMonthCardDO.getParkId().equals(parkingRecordDO.getParkId())){
            isValid = false;
        }
        return isValid;
    }

//    @Override
//    /*
//    * @Description: 接收相机回传发送的车辆图片，上传到华为云obs，并更新车辆进出记录中的车辆图片url字段值
//    * @Author: LZY
//    * @Date: 2021/6/27 11:33
//    * @Params: [image]，传入的图片是base64编码的图像字符串
//    * @Return: com.obs.services.model.PutObjectResult
//    */
//    public PutObjectResult receiveSnapImage(String image) throws BusinessException {
//        return null;
//    }

    @Override
    /*
    * @Description: 创建月卡
    * @Author: LZY
    * @Date: 2021/6/27 11:34
    * @Params: [tollMonthCardDO]
    * @Return: void
    */
    public void createTollMonthCard(TollMonthCardDO tollMonthCardDO) throws BusinessException {

    }

    @Override
    public List<TollMonthCardDO> getTollMonthCard(TollMonthCardQueryDO tollMonthCardQueryDO) throws BusinessException {
        if (tollMonthCardQueryDO == null){
            log.warn("[ParkingService] query toll month card fail, because query condition is null");
            return new LinkedList<>();
        }
        QueryWrapper<TollMonthCardPO> queryWrapper = new QueryWrapper<>();
        String id = tollMonthCardQueryDO.getId();
        queryWrapper.eq(StringUtil.isNotEmpty(id), "id", id);
        String license = tollMonthCardQueryDO.getLicense();
        queryWrapper.eq(StringUtil.isNotEmpty(license), "license", license);
        String buyerId = tollMonthCardQueryDO.getBuyerId();
        queryWrapper.eq(StringUtil.isNotEmpty(buyerId), "buyer_id", buyerId);
        Integer status = tollMonthCardQueryDO.getStatus();
        queryWrapper.eq(status != null, "status", status);
        String parkId = tollMonthCardQueryDO.getParkId();
        queryWrapper.eq(StringUtil.isNotEmpty(parkId), "park_id", parkId);
        List<TollMonthCardPO> tollMonthCardPOS = tollMonthCardDAO.selectList(queryWrapper);

        List<TollMonthCardDO> tollMonthCardDOS = new LinkedList<>();
        for(TollMonthCardPO tollMonthCardPO : tollMonthCardPOS){
            TollMonthCardDO tollMonthCardDO = new TollMonthCardDO();
            BeanUtils.copyProperties(tollMonthCardPO, tollMonthCardDO);
            tollMonthCardDOS.add(tollMonthCardDO);
        }

        return tollMonthCardDOS;
    }

    @Override
    public void updateTollMonthCard(TollMonthCardDO tollMonthCardDO) throws BusinessException {

    }

    @Override
    public ParkingQRCodeDO generateParkingQRCode(String content, String spaceId, String deviceId) throws BusinessException {
        String qrCode = QRCodeUtil.generateQRCode(content);
        if (qrCode == null){
            throw new BusinessException(BusinessException.SERVERERROR, "generate qr code fail");
        }
        QueryWrapper<ParkingQRCodePO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("space_id", spaceId).eq("is_deleted", 0);
        Integer count = parkingQRCodeDAO.selectCount(queryWrapper);
        if (count > 0){
            log.info("space {} have parking qr code", spaceId);
            throw new BusinessException(BusinessException.PARAMETERERROR, "space parking qr code is exist");
        }
        ParkingQRCodePO parkingQRCodePO = new ParkingQRCodePO();
        parkingQRCodePO.setSpaceId(spaceId);
        parkingQRCodePO.setDeviceId(deviceId);
        parkingQRCodePO.setImageStr(qrCode);
        parkingQRCodeDAO.insert(parkingQRCodePO);
        ParkingQRCodeDO parkingQRCodeDO = new ParkingQRCodeDO();
        BeanUtils.copyProperties(parkingQRCodePO, parkingQRCodeDO);
        return parkingQRCodeDO;
    }

    @Override
    public ParkingQRCodeDO getParkingQRCode(String spaceId) throws BusinessException {
        QueryWrapper<ParkingQRCodePO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("space_id", spaceId).eq("is_deleted", 0);
        ParkingQRCodePO parkingQRCodePO = parkingQRCodeDAO.selectOne(queryWrapper);
        if (parkingQRCodePO == null){
            throw new BusinessException(BusinessException.PARAMETERERROR, "parking qr code is not exist");
        }
        ParkingQRCodeDO parkingQRCodeDO = new ParkingQRCodeDO();
        BeanUtils.copyProperties(parkingQRCodePO, parkingQRCodeDO);
        return parkingQRCodeDO;
    }

    @Override
    public void deleteParkingQRCode(String spaceId) throws BusinessException {
        UpdateWrapper<ParkingQRCodePO> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("space_id", spaceId).set("is_deleted", 1);
        parkingQRCodeDAO.update(null, updateWrapper);
    }

    @Override
    public PageVO<ChargeOrderVO> getChargeOrderList(ChargeOrderDTO chargeOrderDTO) {
        int pageSize = chargeOrderDTO.getPageSize();
        int pageNum = chargeOrderDTO.getPageNum();
        ChargeOrderVO chargeOrderVO = new ChargeOrderVO();
        QueryWrapper<TollMonthCardOrderPO>queryWrapper = new QueryWrapper<>();
        String id = chargeOrderDTO.getOrderId();
        queryWrapper.eq("id",id);
        TollMonthCardOrderPO tollMonthCardOrderPO = tollMonthCardOrderDAO.selectOne(queryWrapper);
        BeanUtils.copyProperties(chargeOrderDTO,chargeOrderVO);
        Page<TollMonthCardOrderPO>page = new Page<>(pageNum,pageSize);
        IPage<TollMonthCardOrderPO>tollMonthCardOrderPOIPage = tollMonthCardOrderDAO.selectPage(page,queryWrapper);
        List<ChargeOrderVO>chargeOrderVOS = new ArrayList<>();
        ChargeOrderVO chargeOrderVO1 = new ChargeOrderVO();
        BeanUtils.copyProperties(tollMonthCardOrderPO,chargeOrderVO1);

        //获取inout_id
        QueryWrapper <ParkingOrderPO>queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("license",tollMonthCardOrderPO.getLicense());
        ParkingOrderPO parkingOrderPO = new ParkingOrderPO();
        String inout_id = parkingOrderPO.getInoutId();
        chargeOrderVO1.setInoutId(inout_id);
        chargeOrderVOS.add(chargeOrderVO1);
        PageVO<ChargeOrderVO>chargeOrderVOPageVO = new PageVO<>();
        chargeOrderVOPageVO.setTotal(tollMonthCardOrderPOIPage.getTotal());
        chargeOrderVOPageVO.setPageSize(chargeOrderVOS.size());
        chargeOrderVOPageVO.setPageNum(tollMonthCardOrderPOIPage.getCurrent());
        chargeOrderVOPageVO.setPages(tollMonthCardOrderPOIPage.getPages());
        chargeOrderVOPageVO.setRecords(chargeOrderVOS);
        return chargeOrderVOPageVO;
    }

    @Override
    public PageVO<TollMonthCardOrderPO> getTollMonthCardOrderPOList(String id, int pageSize, int pageNum) {
        QueryWrapper<TollMonthCardOrderPO>orderPOQueryWrapper = new QueryWrapper<>();
        orderPOQueryWrapper.eq("id",id);
        List<TollMonthCardOrderPO> tollMonthCardOrderPOList = tollMonthCardOrderDAO.selectList(orderPOQueryWrapper);
        Page<TollMonthCardOrderPO>page = new Page<>(pageNum,pageSize);
        IPage<TollMonthCardOrderPO>tollMonthCardOrderPOIPage = tollMonthCardOrderDAO.selectPage(page,orderPOQueryWrapper);
        List<TollMonthCardOrderPO>recordPOs = new ArrayList<>();
        /*for(TollMonthCardOrderPO tollMonthCardOrderPO1 : tollMonthCardOrderPOList){
            recordPOs.add(tollMonthCardOrderPO1);
        }*/
        PageVO<TollMonthCardOrderPO>tollMonthCardOrderPOPageVO = new PageVO<>();
        tollMonthCardOrderPOPageVO.setRecords(tollMonthCardOrderPOList);
        tollMonthCardOrderPOPageVO.setPageNum(tollMonthCardOrderPOIPage.getCurrent());
        tollMonthCardOrderPOPageVO.setPages(tollMonthCardOrderPOIPage.getPages());
        tollMonthCardOrderPOPageVO.setTotal(tollMonthCardOrderPOIPage.getTotal());
        tollMonthCardOrderPOPageVO.setPageSize(tollMonthCardOrderPOList.size());
        log.info("[mysql] get tollMonthCardOrderPO {} success",id);
        return tollMonthCardOrderPOPageVO;
    }

    @Override
    public String createDevice(String deviceId, String deviceName) {
        TollIotDetailPO tollIotDetailPO = new TollIotDetailPO();
        tollIotDetailPO.setDeviceId(deviceId);
        tollIotDetailPO.setDeviceName(deviceName);
        int result = tollIotDetailDAO.insert(tollIotDetailPO);
        if (result > 0){
            log.info("[mysql]: create space {} success", tollIotDetailPO.getId());
            return  tollIotDetailPO.getDeviceId();
        }
        log.warn("[mysql]: create space fail");
        throw new BusinessException(BusinessException.SERVERERROR, "[mysql]: create space fail");
    }

    @Override
    public void updateDevice(String deviceId, String deviceName) {
        TollIotDetailPO tollIotDetailPO = new TollIotDetailPO();
        QueryWrapper<TollIotDetailPO>queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("device_id",deviceId);
        tollIotDetailPO = tollIotDetailDAO.selectOne(queryWrapper);
        tollIotDetailPO.setDeviceName(deviceName);
        if(tollIotDetailPO.getId() == null || tollIotDetailPO.getId().equals("")){
            log.warn("[mysql]id can not be empty");
        }
        tollIotDetailDAO.updateById(tollIotDetailPO);
        log.info("[mysql] device having be updated");
    }

    @Override
    public int removeDevice(String id) {
        QueryWrapper<TollIotDetailPO>queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",id);
        TollIotDetailPO tollIotDetailPO = tollIotDetailDAO.selectOne(queryWrapper);
        if(tollIotDetailPO == null){
            log.warn("[mysql] delete not successful");
            throw new BusinessException(BusinessException.SERVERERROR,"FAIL");
        }
        tollIotDetailPO.setIsDeleted(1);
        tollIotDetailDAO.updateById(tollIotDetailPO);
        return 1;
    }

    @Override
    public PageVO<TollIotDetailVO> getDeviceList(int pageSize, int pageNum) {
        QueryWrapper<TollIotDetailPO>queryWrapper = new QueryWrapper<>();
        Page<TollIotDetailPO>page = new Page<>(pageNum,pageSize);
        IPage<TollIotDetailPO>iPage = tollIotDetailDAO.selectPage(page,queryWrapper);
        List<TollIotDetailPO>deviceList = iPage.getRecords();
        List<TollIotDetailVO>tollIotDetailVOS = new ArrayList<>();
        for(TollIotDetailPO tollIotDetailPO:deviceList){
            TollIotDetailVO tollIotDetailVO = new TollIotDetailVO();
            tollIotDetailVO.setId(tollIotDetailPO.getId());
            tollIotDetailVO.setDeviceId(tollIotDetailPO.getDeviceId());
            tollIotDetailVO.setParkId(tollIotDetailPO.getParkId());
            tollIotDetailVO.setDeviceName(tollIotDetailPO.getDeviceName());
            tollIotDetailVO.setParkingAreaId(tollIotDetailVO.getParkingAreaId());
            tollIotDetailVO.setSpaceName(tollIotDetailPO.getSpaceName());
            tollIotDetailVOS.add(tollIotDetailVO);
        }
        PageVO<TollIotDetailVO>pageVO = new PageVO<>();
        pageVO.setPageSize(tollIotDetailVOS.size());
        pageVO.setPageNum(iPage.getCurrent());
        pageVO.setPages(iPage.getPages());
        pageVO.setTotal(iPage.getTotal());
        pageVO.setRecords(tollIotDetailVOS);

        return pageVO;
    }
}
