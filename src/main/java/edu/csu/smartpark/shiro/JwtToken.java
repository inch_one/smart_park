package edu.csu.smartpark.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/*
* @Description: 类似UsernamePasswordToken，用于校验登录状态
* @Author: LZY
* @Date: 2021/4/19 21:28
*/
public class JwtToken implements AuthenticationToken {
    private String jwt;

    public JwtToken(String jwt) {
        this.jwt = jwt;
    }

    //返回的都是jwt
    @Override
    public Object getPrincipal() {
        return jwt;
    }

    @Override
    public Object getCredentials() {
        return jwt;
    }

}
